// Package user defines a User type that's stored in Contexts.
package userContext

import "context"

// UserCtx is the type of value stored in the Contexts.
type UserCtx struct {
    UserName string
    GrantedRole int
}

// key is an unexported type for keys defined in this package.
// This prevents collisions with keys defined in other packages.
type key int

// userKey is the key for userCtx.UserCtx values in Contexts. It is
// unexported; clients use user.NewContext and user.FromContext
// instead of using this key directly.
var userKey key

// NewContext returns a new Context that carries value u.
func NewUserContext(ctx context.Context, userName string, grantedRole int) context.Context {
    var u UserCtx
    u.UserName = userName
    u.GrantedRole = grantedRole

    return context.WithValue(ctx, userKey, &u)
}

// FromContext returns the UserCtx value stored in ctx, if any.
func FromUserContext(ctx context.Context) (*UserCtx, bool) {
    u, ok := ctx.Value(userKey).(*UserCtx)
    return u, ok
}