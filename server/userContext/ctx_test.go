package userContext

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFromUserContext(t *testing.T){
	// empty context
	_, ok := FromUserContext(context.Background())

	assert.False(t, ok, "empty context should return not ok")
}