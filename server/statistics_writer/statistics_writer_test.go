package statistics_writer

import (
	"testing"
	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/settings"
	"url-shortener/store"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestWriteStatistics(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)

	{ // test for error
		id, _ := uuid.Parse("00000028-0009-1965-1975-217000000000")

		err := store.DbWriteStats(id, "Europe", "Germany", "Berlin", "Test Agent", "success", def.ROLES["Viewer"])
		assert.Error(t, err)
	}
	{ // test for alias git
		_, id, _ := store.FindUrl4Alias("git", def.ROLES["God"])

		err := store.DbWriteStats(id, "Europe", "Germany", "Berlin", "Test Agent", "success", def.ROLES["Viewer"])
		assert.NoError(t, err)
	}
}
