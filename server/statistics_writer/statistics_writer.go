package statistics_writer

import (
	"context"
	"fmt"
	"net/http"

	"github.com/google/uuid"

	"url-shortener/ip_statistics"
	"url-shortener/logger"
	"url-shortener/store"
)

func WriteStatistics(ctx context.Context, r *http.Request, id uuid.UUID, resultCode string, role int) error {
	logger.Log.Debug(fmt.Sprintf("WriteStatistics entry, resultCode \"%s\" ", resultCode))
	agent := r.UserAgent()
	ip, _ := ip_statistics.GetIP(r)
	logger.Log.Debug(fmt.Sprintf("IP %s, agent %s", ip, agent))

	ri, _ := ip_statistics.QueryIp(ctx, ip)

	err := store.DbWriteStats(id, ri.Continent, ri.Country, ri.City, agent, resultCode, role)
	if err != nil {
		return err
	}

	return nil
}
