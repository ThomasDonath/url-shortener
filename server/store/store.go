package store

import (
	"crypto/sha256"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/settings"
)

const ERROR_NOT_FOUND string = "alias not found"
const ERROR_NOT_VISIBLE string = "alias is not visible"

var buffer map[string]def.BufferEntryType

// search for the full URL to a given (short) alias
// return empty strings if not found or if the user isn't allowed to get an existing entry
func FindUrl4Alias(shortedUrl string, grantedRole int) (string, uuid.UUID, error) {
	logger.Log.Debug(fmt.Sprintf("FindUrl entry alias:%v role:%d", shortedUrl, grantedRole))

	NIL_UUID, _ := uuid.Parse("00000000-0000-0000-0000-000000000000")
	var retvUrl string
	var retvId uuid.UUID
	var retErr error = nil

	foundEntry, found := buffer[shortedUrl]
	if found {
		logger.Log.Debug(fmt.Sprintf("FindUrl() found in buffer, id=%s public=%v", foundEntry.Id.String(), foundEntry.Public))
		if foundEntry.Public || grantedRole > def.ROLES["Public"] {
			retvUrl = foundEntry.FullUrl
			retvId = foundEntry.Id
		} else {
			retvUrl = ""
			retvId = foundEntry.Id // we need ID to write stats
			retErr = errors.New(ERROR_NOT_VISIBLE)
		}
	} else {
		u, err := DbFindUrlEntry(shortedUrl)
		logger.Log.Debug(fmt.Sprintf("FindUrl() found in DB, public=%v, err=%+v", foundEntry.Public, err))
		if err != nil {
			if err != sql.ErrNoRows {
				logger.Log.Error(fmt.Sprintf("Error %v", err))
			}
			retvUrl = ""
			retvId = NIL_UUID
			retErr = errors.New(ERROR_NOT_FOUND)
		} else {
			if u.Public || grantedRole > def.ROLES["Public"] {
				if len(buffer) > settings.UrlBufferLimit { // to avoid memory overflow keep length < limit
					buffer = nil
					buffer = make(map[string]def.BufferEntryType)
					logger.Log.Warn("truncated URL buffer")
				}
				buffer[u.Alias] = def.BufferEntryType{FullUrl: u.FullUrl, Public: u.Public, Id: u.Id}
				retvUrl = u.FullUrl
				retvId = u.Id
			} else {
				retvUrl = ""
				retvId = u.Id // we need ID to write stats
				retErr = errors.New(ERROR_NOT_VISIBLE)
			}
		}
	}
	return retvUrl, retvId, retErr
}

// GetEntries returns a subset aliases for a paged table at client side
func GetEntries(
	filterUrl string,
	filterAlias string,
	filterOwner string,
	sortColumn string,
	sortOrder string,
	pageNumber uint64,
	pageSize uint64,
	grantedRole int) ([]def.UrlEntryType, uint, error) {
	logger.Log.Debug(fmt.Sprintf("getEntries entry for role %d", grantedRole))

	if grantedRole < def.ROLES["Viewer"] {
		m := "user is not allowed to query entries"

		return nil, 0, errors.New(m)
	}

	data, rowCount, err := dbGetAll(filterUrl, filterAlias, filterOwner, sortColumn, sortOrder, pageNumber, pageSize)
	if err != nil {
		return nil, 0, err
	}
	return data, rowCount, nil
}

func UpdateEntry(e def.UrlEntryType, grantedRole int, userName string) (def.ApiReturnType, error) {
	logger.Log.Debug(fmt.Sprintf("UpdateEntry %v, role %d", e, grantedRole))

	if grantedRole < def.ROLES["Editor"] {
		m := "user is not allowed to update entries"

		return def.NewApiReturnType(def.ErrorCodeForbidden, m, def.NewUrlEntryEmptyType()), errors.New(m)
	}

	err := dbUpdateUrlEntry(e, grantedRole, userName)
	logger.Log.Debug(fmt.Sprintf("UpdateEntry result %v", err))
	if err == nil {
		delete(buffer, e.Alias) //ensure we don't have expired entries in buffer
	} else {
		return def.NewApiReturnType(def.ErrorCodeOk, err.Error(), def.NewUrlEntryEmptyType()), err
	}
	return def.NewApiReturnType(def.ErrorCodeOk, "", e), err
}

func DeleteEntry(id uuid.UUID, grantedRole int, userName string) (def.ApiReturnType, error) {
	logger.Log.Debug(fmt.Sprintf("DeleteEntry entry id:%v role:%d", id, grantedRole))

	if grantedRole < def.ROLES["Editor"] {
		m := "user is not allowed to delete entries"
		return def.NewApiReturnType(def.ErrorCodeForbidden, m, def.NewUrlEntryEmptyType()), errors.New(m)
	}

	a, _ := DbFindALias(id)
	delete(buffer, a)
	dbDeleteEntry(id, grantedRole, userName)
	return def.NewApiReturnType(def.ErrorCodeOk, "", def.NewUrlEntryEmptyType()), nil

}

func GetStatistics(
	grantedRole int,
	id uuid.UUID,
	format string,
	sortColumn string,
	sortOrder string,
	pageNumber uint64,
	pageSize uint64) ([]def.ApiStatisticsType, uint, error) {

	if grantedRole < def.ROLES["Editor"] {
		m := "user is not allowed to query statistics"

		return nil, 0, errors.New(m)
	}

	return DbGetStatistics(
		id,
		format,
		sortColumn,
		sortOrder,
		pageNumber,
		pageSize)
}

func CreateEntry(entry def.UrlEntryType, grantedRole int) (def.ApiReturnType, error) {
	logger.Log.Debug(fmt.Sprintf("CreateEntry entry %v", entry))

	if grantedRole < def.ROLES["Editor"] {
		m := "user is not allowed to create entries"
		return def.NewApiReturnType(def.ErrorCodeForbidden, m, def.NewUrlEntryEmptyType()), errors.New(m)
	}

	var retv def.ApiReturnType

	entry.Id = uuid.New()

	err := dbInsertUrlEntry(entry)
	if err == nil {
		buffer[entry.Alias] = def.BufferEntryType{FullUrl: entry.FullUrl, Public: entry.Public, Id: entry.Id}
		retv = def.NewApiReturnType(def.ErrorCodeOk, "", entry)
	} else {
		retv = def.NewApiReturnType(def.ErrorCodeInsert, fmt.Sprintf("Error %v", err), def.NewUrlEntryEmptyType())
	}
	logger.Log.Debug(fmt.Sprintf("CreateEntry result %v", err))
	return retv, err
}

func CreateNamedEntry(e def.UrlEntryType, grantedRole int) (def.ApiReturnType, error) {
	logger.Log.Debug(fmt.Sprintf("CreateNamedEntry entry %v", e))

	if grantedRole < def.ROLES["Editor"] {
		m := "user is not allowed to create entries"
		return def.NewApiReturnType(def.ErrorCodeForbidden, m, def.NewUrlEntryEmptyType()), errors.New(m)
	}

	existingEntry, _, _ := FindUrl4Alias(e.Alias, def.ROLES["God"]) // want to find *all* entries regardless of public...

	if existingEntry == "" {
		return CreateEntry(e, grantedRole)
	} else {
		return def.NewApiReturnType(def.ErrorCodeAliasInUse, "alias already in use", def.NewUrlEntryEmptyType()), nil
	}
}

func createAlias(url string) (string, error) {
	h := sha256.New()
	h.Write([]byte(url))

	fullHash := fmt.Sprintf("%x", h.Sum(nil))

	i := 0
	for {
		u, _, _ := FindUrl4Alias(fullHash[1:6+i], def.ROLES["God"]) // want to find *all* entries regardless of public...

		if u == "" {
			// found an unused alias
			break
		}
		i++
		if i >= len(fullHash)-7 {
			// no unused alias found
			return "", errors.New("unable to find an alias")
		}
	}

	return fullHash[1 : 6+i], nil
}

func CreateRandomEntry(e def.UrlEntryType, grantedRole int) (def.ApiReturnType, error) {
	logger.Log.Debug(fmt.Sprintf("CreateRandomEntry entry url:%v, Public:%v, Owner: %v, description:%v", e.FullUrl, e.Public, e.Owner, e.Description))

	if grantedRole < def.ROLES["Editor"] {
		m := "user is not allowed to create entries"
		return def.NewApiReturnType(def.ErrorCodeForbidden, m, def.NewUrlEntryEmptyType()), errors.New(m)
	}

	var retv def.ApiReturnType
	alias, err := createAlias(e.FullUrl)
	if err != nil {
		retv = def.NewApiReturnType(def.ErrorCodeInsert, fmt.Sprintf("error %+v", err), def.NewUrlEntryEmptyType())
		return retv, err
	} else {
		retv, err = CreateEntry(def.NewUrlEntryType(e.FullUrl, alias, e.Public, e.Owner, e.ExpiryDate, e.Description), grantedRole)
		retv.Data.Alias = alias
		return retv, err
	}
}

func BufferClear(period int32) {
	logger.Log.Debug(fmt.Sprintf("BufferClear entry period:%v", period))
	for {
		buffer = nil
		buffer = make(map[string]def.BufferEntryType)
		logger.Log.Info("buffer cleared")
		time.Sleep(time.Duration(period) * time.Second)
	}
}

func InitStore(withTestdata bool, withFullSet bool, clearCache bool) error {
	logger.Log.Debug("starting InitStore")

	if err := dbInit(settings.DbHostName, settings.DbName, settings.DbUserName, settings.DbPasswd); err != nil {
		log.Fatalf("Error initializing store %v", err)
	}
	buffer = make(map[string]def.BufferEntryType)

	if withTestdata {
		dbClearTable()
		LoadTestData(withFullSet, clearCache)
	}

	return nil
}

// intialize the buffer with a constant test data set
func LoadTestData(fullSet bool, clearCache bool) {

	if fullSet {
		i := 0
		for i < 300 {
			CreateEntry(def.NewUrlEntryType("https://dummy1.local", fmt.Sprintf("i%d", i), false, "thomas.donath@gematik.de", "", ""), def.ROLES["God"])
			i++
		}
	}
	CreateEntry(def.NewUrlEntryType("https://gitlab.com", "git", false, "thomas.donath@gematik.de", "", ""), def.ROLES["God"])
	if fullSet {
		i := 0
		for i < 300 {
			CreateEntry(def.NewUrlEntryType("https://dummy1.local", fmt.Sprintf("j%d", i), false, "ownerXY", "", ""), def.ROLES["God"])
			i++
		}
	}
	CreateEntry(def.NewUrlEntryType("https://gitlab.com/viewer", "gat", true, "ernie", "", ""), def.ROLES["God"])
	if fullSet {
		i := 0
		for i < 300 {
			CreateEntry(def.NewUrlEntryType("https://dummy1.local", fmt.Sprintf("a%d", i), true, "private", "", ""), def.ROLES["God"])
			i++
		}
	}
	CreateEntry(def.NewUrlEntryType("https://www.got.com/hell/index.html", "got", true, "penguin", "", ""), def.ROLES["God"])
	if fullSet {
		i := 0
		for i < 300 {
			CreateEntry(def.NewUrlEntryType("https://dummy1.local", fmt.Sprintf("d%d", i), true, "penguin", "", ""), def.ROLES["God"])
			i++
		}
	}
	{
		_, id, err := FindUrl4Alias("git", def.ROLES["God"])
		if err != nil {
			log.Panicf("cant find entry git %+v", err)
		}
		i := 0
		for i < 30 {
			DbWriteStats(id, "Europe", "Germany", "Berlin", "Test-Agent", "success", 0)
			i++
		}
	}
	if clearCache {
		for a := range buffer {
			delete(buffer, a)
		}
	}
	logger.Log.Info("Store initialized")
	//logger.Log.Debug(fmt.Sprintf("map: %v", buffer))
}
