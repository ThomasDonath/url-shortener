package store

import (
	"fmt"
	"url-shortener/logger"
)

func createSchema() error {
	{
		_, err := client.Exec(`
	create table if not exists aliases (
		id				varchar(36) not null primary key,
		alias 			varchar(30) not null unique,
		full_url		varchar(2000) not null,
		public 	        boolean not null,
		owner           varchar(60) not null,
		description 	text
	)`)
		if err != nil {
			logger.Log.Debug(fmt.Sprintf("error creating table aliases %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`
		create table if not exists statistics (
			alias 		varchar(30) not null,
			date_time	datetime not null,
			continent 	varchar(100),
			country 	varchar(100),
			city    	varchar(100),
			agent       varchar(100),
			role        int unsigned,
			result      varchar(20),
			counter 	int unsigned not null
			)`)
		if err != nil {
			logger.Log.Debug(fmt.Sprintf("error creating table statistics %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`alter table aliases add column expiry_date date`)
		if err != nil && err.Error() != "Error 1060 (42S21): Duplicate column name 'expiry_date'" {
			logger.Log.Debug(fmt.Sprintf("error adding aliases.expiry_date %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`alter table aliases drop index alias`)
		if err != nil && err.Error() != "Error 1091 (42000): Can't DROP 'alias'; check that column/key exists" {
			logger.Log.Debug(fmt.Sprintf("error droping UK on aliases %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`alter table aliases add constraint aliases_uk unique (alias, expiry_date)`)
		if err != nil && err.Error() != "Error 1061 (42000): Duplicate key name 'aliases_uk'" {
			logger.Log.Debug(fmt.Sprintf("error adding new UK on aliases %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`alter table statistics add aliases_id varchar(36)`)
		if err != nil && err.Error() != "Error 1060 (42S21): Duplicate column name 'aliases_id'" {
			logger.Log.Debug(fmt.Sprintf("error adding statistics.aliases_id %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`
			update statistics as stats
			set stats.aliases_id = (select a.id from aliases a where a.alias = stats.alias)
			where stats.aliases_id is null
		`)
		if err != nil && err.Error() != "Error 1054 (42S22): Unknown column 'stats.alias' in 'where clause'"{
			logger.Log.Debug(fmt.Sprintf("error migrating FKC data %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`
			delete from statistics 
			where aliases_id is null
		`)
		if err != nil {
			logger.Log.Debug(fmt.Sprintf("error delete not found entries %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`alter table statistics modify aliases_id varchar(36) not null`)
		if err != nil && err.Error() != "Error 1060 (42S21): Duplicate column name 'expiry_date'" {
			logger.Log.Debug(fmt.Sprintf("error adding NN on statistics.aliases_id %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`alter table statistics drop column alias`)
		if err != nil && err.Error() != "Error 1091 (42000): Can't DROP 'alias'; check that column/key exists" {
			logger.Log.Debug(fmt.Sprintf("error droping statistics.alias %v", err))
			return err
		}
	}
	{
		_, err := client.Exec(`alter table statistics add CONSTRAINT statistics_aliases_fk foreign key (aliases_id) references aliases(id) on delete cascade`)
		if err != nil && err.Error() != "Error 1826 (HY000): Duplicate foreign key constraint name 'statistics_aliases_fk'" {
			logger.Log.Debug(fmt.Sprintf("error creating FKC %v", err))
			return err
		}
	}

	return nil
}
