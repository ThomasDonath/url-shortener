package store

// https://github.com/go-sql-driver/mysql

import (
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/google/uuid"
	sani "github.com/mrz1836/go-sanitize"

	"url-shortener/def"
	"url-shortener/logger"

	_ "github.com/go-sql-driver/mysql"
)

var client *sql.DB

func DbClose() {
	client.Close()
}

// open the database during server startup *and* prepares the tables
func dbInit(hostname string, dbName string, dbUserName string, password string) error {
	logger.Log.Debug("connecting to database on host " + hostname)

	var err error
	client, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?clientFoundRows=true", dbUserName, password, hostname, dbName))
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error starting client %+v", err))
		return err
	}
	client.SetConnMaxLifetime(time.Minute * 3)
	client.SetMaxOpenConns(10)
	client.SetMaxIdleConns(10)

	// we have to wait for DB startup
	tries := 0
	for {
		err = client.Ping()
		if err != nil {
			if tries > 10 {
				logger.Log.Debug(fmt.Sprintf("error connecting to database: %+v", err))
				return err
			} else {
				time.Sleep(1 * time.Second)
				tries++
			}
		} else {
			break
		}
	}
	logger.Log.Debug("connected to database")

	//
	// prepare database schema
	// important! if we want to change this we should do it with alter table... after initial create statements to maintain existing instances
	err2 := createSchema()

	return err2
}

func DbFindUrlEntry(alias string) (def.UrlEntryType, error) {
	logger.Log.Debug(fmt.Sprintf("dbFindUrlEntry() entry %v", alias))
	var urlEntry def.UrlEntryType
	stmt, err := client.Prepare(`
			SELECT id, alias, full_url, public, owner, description 
			FROM aliases 
			WHERE alias = ?
			AND   (expiry_date > SYSDATE() or expiry_date is null)`)
	if err != nil {
		return urlEntry, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(alias).Scan(&urlEntry.Id, &urlEntry.Alias, &urlEntry.FullUrl, &urlEntry.Public, &urlEntry.Owner, &urlEntry.Description)
	if err != nil && err != sql.ErrNoRows {
		return urlEntry, err
	}

	logger.Log.Debug(fmt.Sprintf("dbFindUrlEntry() result: alias %v err %v", urlEntry, err))
	return urlEntry, err
}

func DbFindALias(id uuid.UUID) (string, error) {
	logger.Log.Debug(fmt.Sprintf("DbFindALias() entry id %v", id))
	var alias string
	stmt, err := client.Prepare(`
			SELECT alias
			FROM aliases 
			WHERE id = ?`)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("dbFindALias() error preparing SQL %+v", err))
		return "", err
	}
	defer stmt.Close()

	err = stmt.QueryRow(id).Scan(&alias)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("dbFindALias() error fetching SQL %+v", err))
		return "", err
	}

	logger.Log.Debug(fmt.Sprintf("DbFindALias() result: alias %v", alias))
	return alias, nil
}

func dbInsertUrlEntry(alias def.UrlEntryType) error {
	logger.Log.Debug("dbInsertUrlEntry() entry")

	stmt, err := client.Prepare(`
			INSERT INTO aliases (
				id, alias, full_url, public, owner, expiry_date, description
			) VALUES (?, ?, ?, ?, ?, if(?='',null, substring(?, 1, 10)), ?)`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		alias.Id,
		alias.Alias,
		alias.FullUrl,
		alias.Public,
		alias.Owner,
		alias.ExpiryDate,
		alias.ExpiryDate,
		alias.Description,
	)

	return err
}

func dbUpdateUrlEntry(alias def.UrlEntryType, grantedRole int, userName string) error {
	logger.Log.Debug("dbUpdateUrlEntry() entry")

	stmt, err := client.Prepare(fmt.Sprintf(`
		UPDATE aliases 
		set alias = ?,
		    full_url = ?,
		    public = ?, 
			owner = ?, 
			expiry_date = if(?='', null, substring(?, 1, 10)),
			description = ?
		where id = ?
		and   (owner = ? or ? = %d)`, def.ROLES["Administrator"]))
	if err != nil {
		return err
	}
	defer stmt.Close()

	result, err := stmt.Exec(
		alias.Alias,
		alias.FullUrl,
		alias.Public,
		alias.Owner,
		alias.ExpiryDate,
		alias.ExpiryDate,
		alias.Description,
		alias.Id,
		userName,
		grantedRole,
	)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error during update for %s: %+v", alias.Alias, err))
		return err
	}
	if numRows, _ := result.RowsAffected(); numRows == 0 {
		logger.Log.Warn(fmt.Sprintf("no rows updated for %s", alias.Alias))
		return errors.New("no rows updated")
	}

	return nil
}

func dbDeleteEntry(id uuid.UUID, grantedRole int, userName string) error {
	logger.Log.Debug("dbDeleteEntry() entry")

	result, err := client.Exec(fmt.Sprintf(`
			delete from aliases 
			where id = ?
			and   (owner = ? or ? = %d)`, def.ROLES["Administrator"]),
		id, userName, grantedRole)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error deleting %s: %+v", id, err))
		return err
	}
	// handle if not found because of wrong user
	numRows, err := result.RowsAffected()
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error deleting %s: %+v", id, err))
		return err
	} else {
		if numRows == 0 {
			logger.Log.Debug(fmt.Sprintf("no rows deleted for alias %s", id))
			return errors.New("delete found no rows")
		}
	}

	return nil
}

func concatWhere(where string, inp string) string {
	var retv string
	if inp != "" {
		if where == "" {
			retv = "WHERE " + inp
		} else {
			retv = where + " AND " + inp
		}
	}
	return retv
}

func computeWhere(
	filterUrl string,
	filterAlias string,
	filterOwner string) string {
	var whereClause string
	if filterAlias != "" {
		whereClause = concatWhere(whereClause, fmt.Sprintf(`alias like "%s%%"`, sani.AlphaNumeric(filterAlias, false)))
	}
	if filterOwner != "" {
		whereClause = concatWhere(whereClause, fmt.Sprintf(`owner like "%s%%"`, sani.Email(filterOwner, true)))
	}
	if filterUrl != "" {
		whereClause = concatWhere(whereClause, fmt.Sprintf(`full_url like "%s%%"`, sani.URL(filterUrl)))
	}
	
	return whereClause
}

func computeSortOrder(inp string) string {
	// we cant trust input as it comes from REST-URL
	var retv string

	switch inp {
	case "asc":
		retv = "asc"
	case "desc":
		retv = "desc"
	default:
		retv = "asc"
	}
	return retv
}

func computeSortColumnAlias(inp string) string {
	// we cant trust input as it comes from REST-URL
	var retv string

	switch inp {
	case "alias":
		retv = inp
	case "owner":
		retv = inp
	default:
		retv = "full_url"
	}
	return retv
}

func computeSortColumnStatistics(inp string) string {
	// we cant trust input as it comes from REST-URL
	var retv string

	switch inp {
	case "1":
		retv = inp
	case "2":
		retv = inp
	default:
		retv = "1"
	}

	return retv
}

func dbGetAll(
	filterUrl string,
	filterAlias string,
	filterOwner string,
	sortColumn string,
	sortOrder string,
	pageNumber uint64,
	pageSize uint64,
) ([]def.UrlEntryType, uint, error) {

	logger.Log.Debug("dbGetAll() entry")

	whereClause := computeWhere(filterUrl, filterAlias, filterOwner)

	if pageSize == 0 {
		pageSize = 20
	}
	pageOffset := pageNumber * pageSize

	var sqlErr error
	var results *sql.Rows

	results, sqlErr = client.Query(fmt.Sprintf(`
		select id, alias, full_url, public, owner, if(expiry_date is null, '', expiry_date), description 
		from aliases %s
		order by %s %s
		limit ?
		offset ?`, whereClause, computeSortColumnAlias(sortColumn), computeSortOrder(sortOrder)), pageSize, pageOffset)

	if sqlErr != nil {
		logger.Log.Error(fmt.Sprintf("error loading table data %+v for %v", sqlErr, whereClause))
		return nil, 0, sqlErr
	}
	logger.Log.Debug(fmt.Sprintf("query for %v", whereClause))

	var rowCount uint
	{
		result := client.QueryRow(fmt.Sprintf(`select count(*) from aliases %s`, whereClause))

		err := result.Scan(&rowCount)
		if err != nil {
			if err == sql.ErrNoRows {
				rowCount = 0
			} else {
				logger.Log.Error(fmt.Sprintf("error get aliases count %+v", err))
				return nil, 0, err
			}
		}
		logger.Log.Debug(fmt.Sprintf("got getNumRows %d", rowCount))
	}

	var aliases []def.UrlEntryType
	for results.Next() {
		var row def.UrlEntryType
		err := results.Scan(&row.Id, &row.Alias, &row.FullUrl, &row.Public, &row.Owner, &row.ExpiryDate, &row.Description)
		if err != nil {
			logger.Log.Error(fmt.Sprintf("error reading data row %+v", err))
			return nil, 0, err
		}
		aliases = append(aliases, row)
	}

	logger.Log.Debug(fmt.Sprintf("read %v rows from database", len(aliases)))

	return aliases, rowCount, nil
}

/*
return statistics for an given alias over time (stats, rowCount, err)
date/time will be rounded as given by param format TOTALS "year", "month", "day", "hour", "none"
*/
func DbGetStatistics(id uuid.UUID, format string, sortColumn string, sortOrder string, pageNumber uint64, pageSize uint64) ([]def.ApiStatisticsType, uint, error) {
	logger.Log.Debug(fmt.Sprintf("DbGetStatistics() entry for alias %s, format %s", id, format))

	const TOTALS = "totals"
	var date_time_format string

	switch format {
	case TOTALS:
		date_time_format = ""
	case "year":
		date_time_format = "%Y-00-00 00:00"
	case "month":
		date_time_format = "%Y-%m-00 00:00"
	case "day":
		date_time_format = "%Y-%m-%d 00:00"
	case "hour":
		date_time_format = "%Y-%m-%d %H:00"
	default:
		date_time_format = "%Y-%m-%d %H:%i"
	}
	if pageSize == 0 {
		pageSize = 20
	}
	pageOffset := pageNumber * pageSize
	var sqlTextAlias, sqlTextDetails string

	if format == TOTALS {
		sqlTextAlias = fmt.Sprintf(`
			select 
			'1800-01-01 00:00', sum(s.counter)
			from statistics s
			where s.aliases_id = ?
			group by ""   # to ensure to get a row even for an empty table
			order by %s %s
			limit ?
			offset ?`, computeSortColumnStatistics(sortColumn), computeSortOrder(sortOrder))
	} else {
		sqlTextAlias = fmt.Sprintf(`
			select 
			date_format(s.date_time, '%s'), sum(s.counter)
			from statistics s
			where s.aliases_id = ?
			group by date_format(s.date_time, '%s')
			order by %s %s
			limit ?
			offset ?`, date_time_format, date_time_format, computeSortColumnStatistics(sortColumn), computeSortOrder(sortOrder))
	}

	if format == TOTALS {
		sqlTextDetails = `
			select 
				s.continent, 
				s.country, 
				s.city, 
				s.agent,
				s.role,
				s.result,
				sum(s.counter)
			from statistics s
			where s.aliases_id = ?
			group by 
				s.continent, 
				s.country, 
				s.city, 
				s.agent,
				s.role,
				s.result
			order by 1, 2, 3, 4, 5, 6`
	} else {
		sqlTextDetails = fmt.Sprintf(`
			select 
				s.continent, 
				s.country, 
				s.city, 
				s.agent,
				s.role,
				s.result,
				sum(s.counter)
			from statistics s
			where s.aliases_id = ?
			and   date_format(s.date_time, '%s') = ?
			group by 
				s.continent, 
				s.country, 
				s.city, 
				s.agent,
				s.role,
				s.result
			order by 1, 2, 3, 4, 5, 6`, date_time_format)
	}

	var sqlErrAlias, sqlErrDetails error
	var resultsAlias, resultsDetails *sql.Rows

	resultsAlias, sqlErrAlias = client.Query(sqlTextAlias, id, pageSize, pageOffset)
	if sqlErrAlias != nil {
		logger.Log.Error(fmt.Sprintf("error loading alias data %+v, %+v", sqlErrAlias, sqlTextAlias))
		return nil, 0, sqlErrAlias
	}

	var stats []def.ApiStatisticsType
	for resultsAlias.Next() {
		var rowAlias def.ApiStatisticsType

		err := resultsAlias.Scan(&rowAlias.Date_time, &rowAlias.Count)
		if err != nil {
			logger.Log.Error(fmt.Sprintf("error reading data row %+v", err))
			return nil, 0, err
		}
		var rowDetails def.ApiStatisticsDetailsType
		if format == TOTALS {
			resultsDetails, sqlErrDetails = client.Query(sqlTextDetails, id)
		} else {
			resultsDetails, sqlErrDetails = client.Query(sqlTextDetails, id, &rowAlias.Date_time)
		}
		for resultsDetails.Next() {
			if sqlErrDetails != nil {
				logger.Log.Error(fmt.Sprintf("error loading details data %+v, %+v", sqlErrDetails, sqlTextDetails))
				return nil, 0, sqlErrAlias
			}
			errD := resultsDetails.Scan(&rowDetails.Continent, &rowDetails.Country, &rowDetails.City, &rowDetails.Agent, &rowDetails.Role, &rowDetails.Result, &rowDetails.Count)
			if errD != nil {
				logger.Log.Error(fmt.Sprintf("error reading data details row %+v", err))
				return nil, 0, errD
			}

			_role, _ := strconv.ParseInt(rowDetails.Role, 10, 0)
			switch int(_role) {
			case def.ROLES["Public"]:
				rowDetails.Role = "Public"
			case def.ROLES["Viewer"]:
				rowDetails.Role = "Viewer"
			case def.ROLES["Editor"]:
				rowDetails.Role = "Editor"
			case def.ROLES["Administrator"]:
				rowDetails.Role = "Administrator"
			default:
				rowDetails.Role = "unknown"
			}
			rowAlias.Details = append(rowAlias.Details, rowDetails)
		}
		rowAlias.Id = id
		stats = append(stats, rowAlias)
	}

	var rowCount uint
	{
		var result *sql.Row

		if format == TOTALS {
			rowCount = 1
		} else {
			result = client.QueryRow(fmt.Sprintf(`
			select count(*) from (
				select date_format(date_time, '%s') from statistics where aliases_id = ? group by date_format(date_time, '%s')) sub`,
				date_time_format, date_time_format),
				id)
			err := result.Scan(&rowCount)
			if err != nil {
				if err == sql.ErrNoRows {
					rowCount = 0
				} else {
					logger.Log.Error(fmt.Sprintf("error get stats count %+v", err))
					return nil, 0, err
				}
			}
		}
	}

	logger.Log.Debug(fmt.Sprintf("got DbGetStatistics %d/%d", len(stats), rowCount))
	return stats, rowCount, nil
}

func dbClearTable() {
	logger.Log.Debug("dbClearTable() entry")

	_, err := client.Exec("delete from aliases")

	if err != nil {
		logger.Log.Fatal(fmt.Sprintf("error deleting table %+v", err))
	}
}

func DbWriteStats(id uuid.UUID, continent string, country string, city string, agent string, result string, role int) error {
	logger.Log.Debug("dbWriteStats() entry id " + id.String())

	stmt, err := client.Prepare(`
			INSERT INTO statistics (
				aliases_id, 
				date_time, 
				continent, 
				country, 
				city, 
				agent, 
				role, 
				result, 
				counter)
			VALUES (
				?, 
				sysdate(), 
				substr(?, 1, 100), 
				substr(?, 1, 100), 
				substr(?, 1, 100),
				substr(?, 1, 100),
				?, 
				?, 
				?)`)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("dbWriteStats() parse error %+v", err))
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		id,
		continent,
		country,
		city,
		agent,
		role,
		result,
		1,
	)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("dbWriteStats() exec error %+v", err))
		return err
	}
	return nil
}

func DbPing() bool {
	err := client.Ping()
	if err != nil {
		logger.Log.Error(fmt.Sprintf("health check error (db ping) %+v", err))
		return false
	}
	return true
}
