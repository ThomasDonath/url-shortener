package store

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/settings"
)

func TestComputeSortOrder(t *testing.T) {
	assert.Equal(t, "asc", computeSortOrder(""))
	assert.Equal(t, "asc", computeSortOrder("asc"))
	assert.Equal(t, "desc", computeSortOrder("desc"))
	assert.Equal(t, "asc", computeSortOrder("asc'"))
}

func TestComputeSortColumnAlias(t *testing.T) {
	assert.Equal(t, "alias", computeSortColumnAlias("alias"))
	assert.Equal(t, "owner", computeSortColumnAlias("owner"))
	assert.Equal(t, "full_url", computeSortColumnAlias("full_url"))
	assert.Equal(t, "full_url", computeSortColumnAlias(""))
	assert.Equal(t, "full_url", computeSortColumnAlias("Alias"))
	assert.Equal(t, "full_url", computeSortColumnAlias("blabla"))
}

func TestComputeSortColumnStatistics(t *testing.T) {
	assert.Equal(t, "1", computeSortColumnStatistics("1"))
	assert.Equal(t, "2", computeSortColumnStatistics("2"))
	assert.Equal(t, "1", computeSortColumnStatistics(""))
	assert.Equal(t, "1", computeSortColumnStatistics("blabla"))
}

func TestCreateSchema(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(false, false, true)

	err := createSchema()
	assert.NoError(t, err)

	err = createSchema()
	assert.NoError(t, err)
}

func TestInitStore4test(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, false, false)

	assert.Equal(t, 3, len(buffer))
}

func TestTruncateBuffer(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, true, true)

	assert.Equal(t, 0, len(buffer))
	settings.UrlBufferLimit = 100

	i := 0
	for i < 300 {
		FindUrl4Alias(fmt.Sprintf("a%d", i), def.ROLES["God"])
		i++
	}
	assert.LessOrEqual(t, len(buffer), 100)
}

func TestCreateNamedEntry(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, false, true)

	_, err := CreateNamedEntry(def.NewUrlEntryType("https://urltest.example.local", "xyz", false, "ernie", "", "created for campaign bla bla"), def.ROLES["God"])
	assert.NoError(t, err)
	u, _, _ := FindUrl4Alias("xyz", def.ROLES["Editor"])
	assert.Equal(t, "https://urltest.example.local", u, "created entry should be found")
}

func TestCreateRandomEntry(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, false, true)

	retv1, err := CreateRandomEntry(def.NewUrlEntryType("https://urltest.example.local", "", false, "ernie", "1965-09-28", "created for campaign bla bla"), def.ROLES["God"])
	assert.NoError(t, err)
	u, _, _ := FindUrl4Alias(retv1.Data.Alias, def.ROLES["Editor"])
	assert.Equal(t, "https://urltest.example.local", u, "created entry should be found")

	retv2, err := CreateRandomEntry(def.NewUrlEntryType("https://urltest.example.local", "", false, "ernie", "", "created for another campaign"), def.ROLES["God"])
	assert.NoError(t, err)
	assert.NotEqual(t, retv1.Data.Alias, retv2.Data.Alias, "should create another alias")
	u, _, _ = FindUrl4Alias(retv2.Data.Alias, def.ROLES["Editor"])
	assert.Equal(t, "https://urltest.example.local", u, "created entry should be found")
}

func TestFindUrl4Alias(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, false, true)

	u, id, _ := FindUrl4Alias("nono", 100)
	assert.Empty(t, u, "test with not existing entry and level 100")
	assert.Empty(t, id, "test with not existing entry and level 100")
	u, id, _ = FindUrl4Alias("nono", def.ROLES["Public"])
	assert.Empty(t, u, "test with not existing entry queired as Public")
	assert.Empty(t, id, "test with not existing entry queired as Public")

	u, id, _ = FindUrl4Alias("git", 100)
	assert.Equal(t, "https://gitlab.com", u, "existing entry with level 100")
	assert.NotEmpty(t, id, "existing entry with level 100")
	u, _, _ = FindUrl4Alias("git", def.ROLES["Editor"])
	assert.Equal(t, "https://gitlab.com", u, "existing private entry queried as Editor")
	u, id, _ = FindUrl4Alias("git", def.ROLES["Public"])
	assert.Empty(t, u, "existing private entry queried as Public")
	assert.NotEmpty(t, id, "existing private entry queried as Public") // needed for stats

	u, _, _ = FindUrl4Alias("gat", def.ROLES["Administrator"])
	assert.Equal(t, "https://gitlab.com/viewer", u, "existing public entry queried as Administrator")
	u, _, _ = FindUrl4Alias("gat", def.ROLES["Editor"])
	assert.Equal(t, "https://gitlab.com/viewer", u, "existing public entry queried as Editor")
	u, _, _ = FindUrl4Alias("gat", def.ROLES["Public"])
	assert.Equal(t, "https://gitlab.com/viewer", u, "existing public entry queried as Public")

	u, _, _ = FindUrl4Alias("got", def.ROLES["Administrator"])
	assert.Equal(t, "https://www.got.com/hell/index.html", u, "existing public entry queried as Adminitrator")
	u, _, _ = FindUrl4Alias("got", def.ROLES["Editor"])
	assert.Equal(t, "https://www.got.com/hell/index.html", u, "existing public entry queried as Editor")
	u, _, _ = FindUrl4Alias("got", def.ROLES["Public"])
	assert.Equal(t, "https://www.got.com/hell/index.html", u, "existing public entry queried as Public")
}

func TestDbFindALias(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, false, true)

	{ // test for non existent
		id, _ := uuid.Parse("00000000-0000-0000-0000-000000000000")
		a, err := DbFindALias(id)
		assert.Error(t, err)
		assert.Empty(t, a)
	}
	{ // test for a private entry
		_, id, _ := FindUrl4Alias("git", def.ROLES["God"])
		a, err := DbFindALias(id)
		assert.NoError(t, err)
		assert.Equal(t, "git", a)
	}
}

func TestGetEntries(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, true, true)
	// FIXME TestGetEntries: filter, sort-order...

	var lastValue string
	// fetch 1st page
	b1, c1, err1 := GetEntries("", "", "", "alias", "asc", 0, 10, def.ROLES["God"])
	assert.NoError(t, err1)
	assert.Equal(t, uint(1203), c1)
	lastValue = b1[9].Alias

	// fetch 2d page, alias name (generated) should be grater than last at 1st page
	b2, c2, err2 := GetEntries("", "", "", "alias", "asc", 1, 10, def.ROLES["God"])
	assert.NoError(t, err2)
	assert.Equal(t, 10, len(b2))
	assert.Equal(t, c1, c2)
	assert.Less(t, lastValue, b2[0].Alias)
}

func TestUpdateEntry(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, false, true)

	e, err := DbFindUrlEntry("git")
	assert.NoError(t, err)
	e.Public = !e.Public
	{
		_, err := UpdateEntry(e, def.ROLES["Public"], "anonymous")
		if assert.Error(t, err) {
			assert.Equal(t, "user is not allowed to update entries", err.Error())
		}
	}
	{
		_, err := UpdateEntry(e, def.ROLES["Viewer"], "anonymous")
		if assert.Error(t, err) {
			assert.Equal(t, "user is not allowed to update entries", err.Error())
		}
	}
	{
		_, err := UpdateEntry(e, def.ROLES["Editor"], "anotherOne")
		if assert.Error(t, err) {
			assert.Equal(t, "no rows updated", err.Error())
		}
	}
	{
		_, err := UpdateEntry(e, def.ROLES["Editor"], "thomas.donath@gematik.de")
		assert.NoError(t, err)
	}
	{
		e.Public = !e.Public
		_, err := UpdateEntry(e, def.ROLES["Administrator"], "anotherOne")
		assert.NoError(t, err)
	}
}

func TestDeleteEntry(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitStore(true, false, true)

	idNono, _ := uuid.Parse("00000000-0000-0000-0000-000000000000")
	_, err := DeleteEntry(idNono, def.ROLES["Public"], "")
	assert.Error(t, err)
	if assert.Error(t, err) {
		assert.Equal(t, "user is not allowed to delete entries", err.Error())
	}

	_, err = DeleteEntry(idNono, def.ROLES["Administrator"], "")
	assert.NoError(t, err)

	_, err = DeleteEntry(idNono, def.ROLES["Editor"], "")
	assert.NoError(t, err)

	v, id, _ := FindUrl4Alias("gat", 100)
	if v == "" {
		panic("excpected entry not found")
	}
	{
		_, err = DeleteEntry(id, def.ROLES["Editor"], "ernie") // delete an own entry
		assert.NoError(t, err)
		u, _, _ := FindUrl4Alias("gat", 100)
		assert.Empty(t, u, "deleted entry should not be found after that")
		_, err = DeleteEntry(id, def.ROLES["Editor"], "ernie") // delete should be idempotent and raise no error if row does not exist
		assert.NoError(t, err)
	}
	/* FIXME: implement and test if another user with Editor isn't able to delete? But with Admin it should? but no user with < Editor?
	{
		_, _, err = DeleteEntry("git", def.ROLES["Editor"], "ernie")
		if assert.Error(t, err) {
			assert.Equal(t, "", err)
		}
	}*/
}

func TestDbGetStatistics(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()

	id, _ := uuid.Parse("00000028-0009-1965-1975-217000000000")

	res, cnt, err := DbGetStatistics(id, "year", "1", "asc", 0, 1000)
	assert.EqualValues(t, uint64(0), len(res), "should get no rows for year")
	assert.EqualValues(t, uint64(0), cnt, "should get rowcount 0 for year")
	assert.NoError(t, err, "should get statistics for year without error")

	res, cnt, err = DbGetStatistics(id, "totals", "1", "asc", 0, 1000)
	assert.EqualValues(t, uint64(0), len(res), "should get no rows for total")
	assert.EqualValues(t, uint64(1), cnt, "should get rowcount 0 for total")
	assert.NoError(t, err, "should get statistics for total without error")
}
