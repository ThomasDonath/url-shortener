package ip_statistics

import (
	"fmt"
	"net"

	"github.com/oschwald/geoip2-golang"

	"url-shortener/logger"
)

// we open/close the database *once* per server instance (main.go)
var geoDB *geoip2.Reader

func OpenDatabase(path string) {
	var err error
	geoDB, err = geoip2.Open(path + "GeoLite2-City.mmdb")
	if err != nil {
		logger.Log.Fatal(fmt.Sprintf("error opening IP database %+v", err))
	}
}

func CloseDatabase() {
	geoDB.Close()
}

// https://pkg.go.dev/github.com/oschwald/geoip2-golang#section-readme
func DbGetRegion(ip string) (RegionInfo, error) {
	logger.Log.Debug(fmt.Sprintf("DbGetRegion() entry fpr ip %s", ip))
	
	// If you are using strings that may be invalid, check that ip is not nil
	ip_ := net.ParseIP(ip)
	if ip_ == nil || ip_.String() == "127.0.0.1" || ip_.String() == "::1"{
		return emptyInfo, nil
	}

	record, err := geoDB.City(ip_)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error querying IP database %+v", err))
		return emptyInfo, err
	}
	var retv RegionInfo

	retv.Continent = record.Continent.Names["en"]
	retv.Country = record.Country.Names["en"]
	retv.City = record.City.Names["en"]

	//logger.Log.Debug(fmt.Sprintf("Region: %s, %s, %s", retv.Continent, retv.Country, retv.City))
	return retv, nil
}
