package ip_statistics

import (
	"fmt"
	"testing"

	"url-shortener/logger"
	"url-shortener/settings"

	"github.com/stretchr/testify/assert"
)

func TestQueryIp(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	
	settings.GeoIpUseApi = false
	settings.IpBufferLimit = 10

	Init("../")
	defer Shutdown()
	
	i := 1
	for i < settings.IpBufferLimit * 2 {
		QueryIp(nil, fmt.Sprintf("212.86.54.%d", i))
		i++
	}
	assert.LessOrEqual(t,len(ipBuffer), settings.IpBufferLimit)
}