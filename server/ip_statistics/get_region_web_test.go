package ip_statistics

import (
	"os"
	"testing"

	"url-shortener/logger"
	"url-shortener/settings"

	"github.com/stretchr/testify/assert"
)

func TestQueryApi(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	InitGeoApi()

	i, err := QueryApi(nil, os.Getenv("MY_PUBLIC_IP"))
	assert.NoError(t, err)
	assert.Equal(t, "Europe", i.Continent)
	assert.Equal(t, "Germany", i.Country)
	assert.Equal(t, os.Getenv("MY_CITY"), i.City)


	i, err = QueryApi(nil, "127.0.0.1")
	assert.NoError(t, err)
	assert.Equal(t, "", i.City)
}