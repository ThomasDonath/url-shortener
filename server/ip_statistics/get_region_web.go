package ip_statistics

import (
	"context"
	"fmt"

	"github.com/ThomasDonath/geoip2"

	"url-shortener/logger"
	"url-shortener/settings"
)

var GeoApi *geoip2.Api

func InitGeoApi() {
	if settings.GeoApiAccount == "" || settings.GeoApiLicenseKey == "" {
		logger.Log.Panic("unable to initialize API as credentials are empty")
	}
	if settings.GeoIpLite {
		geoip2.UseFreeApi = true
	}
	GeoApi = geoip2.New(settings.GeoApiAccount, settings.GeoApiLicenseKey)
}

func QueryApi(ctx context.Context, ip string) (RegionInfo, error) {
	logger.Log.Debug(fmt.Sprintf("QueryApi() entry fpr ip %s", ip))

	var retv RegionInfo

	if ip == "" || ip == "127.0.0.1" || ip == "::1" {
		return emptyInfo, nil
	}

	resp, err := GeoApi.City(ctx, ip)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error fetching API for %s %+v", ip, err))
	}
	retv.Continent = resp.Continent.Names["en"]
	retv.Country = resp.Country.Names["en"]
	retv.City = resp.City.Names["en"]

	return retv, err
}
