package ip_statistics

import "url-shortener/settings"

func Init(dbPath string) {
	InitIpCache()

	if settings.GeoIpUseApi {
		InitGeoApi()
	} else {
		OpenDatabase(dbPath)
	}
}
func Shutdown() {
	if !settings.GeoIpUseApi {
		CloseDatabase()
	}
}
