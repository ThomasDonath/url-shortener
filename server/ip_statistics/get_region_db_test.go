package ip_statistics

import (
	"os"
	"testing"
	"url-shortener/logger"

	"github.com/stretchr/testify/assert"
)

func TestGetRegion(t *testing.T) {
	logger.InitializeZapCustomLogger()

	OpenDatabase("../")
	defer CloseDatabase()
	{
		r, err := DbGetRegion(os.Getenv("MY_PUBLIC_IP"))
		assert.NoError(t, err)
		assert.Equal(t, "Europe", r.Continent)
		assert.Equal(t, "Germany", r.Country)
		assert.Equal(t, os.Getenv("MY_CITY"), r.City)
	}
	{
		r, err := DbGetRegion("127.0.0.1")
		assert.NoError(t, err)
		assert.Equal(t, "", r.City)
	}
	{
		r, err := DbGetRegion("")
		assert.NoError(t, err)
		assert.Equal(t, "", r.City)
	}
}
