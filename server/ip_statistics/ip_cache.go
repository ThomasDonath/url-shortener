package ip_statistics

import (
	"context"
	"fmt"
	"time"
	"url-shortener/logger"
	"url-shortener/settings"
)

type RegionInfo struct {
	Continent string
	Country   string
	City      string
}

var emptyInfo RegionInfo = RegionInfo{"", "", ""}

var ipBuffer map[string]RegionInfo

func InitIpCache(){
	ipBuffer = make(map[string]RegionInfo)
}

func QueryIp(ctx context.Context, ip string) (RegionInfo, error) {
	logger.Log.Debug(fmt.Sprintf("QueryIp() entry for ip %s", ip))

	var retv RegionInfo
	var err error

	foundEntry, found := ipBuffer[ip]

	if found {
		logger.Log.Debug("found in cache")
		return foundEntry, nil
	} else {
		if settings.GeoIpUseApi {
			retv, err = QueryApi(ctx, ip)
		} else {
			retv, err = DbGetRegion(ip)
		}
		if err != nil {
			logger.Log.Warn("no entry found for ip")
			return emptyInfo, err
		}
		logger.Log.Debug("found in API/DB")

		if len (ipBuffer) > settings.IpBufferLimit {
			logger.Log.Warn("truncated IP buffer")
			ipBuffer = nil
			ipBuffer = make(map[string]RegionInfo)
		}
		ipBuffer[ip] = retv
		return retv, nil
	}
}

func ClearIpCache(period int32) {
	logger.Log.Debug(fmt.Sprintf("ClearIpCache entry period:%v", period))
	for {
		ipBuffer = nil
		ipBuffer = make(map[string]RegionInfo)
		logger.Log.Info("ip buffer cleared")
		time.Sleep(time.Duration(period) * time.Second)
	}
}