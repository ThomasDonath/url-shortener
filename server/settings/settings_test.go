package settings

import (
	"testing"
	"url-shortener/logger"

	"github.com/stretchr/testify/assert"
)

func TestInitializeSettings(t *testing.T) {
	logger.InitializeZapCustomLogger()
	InitializeSettings()
	assert.Equal(t, "127.0.0.1", DbHostName)
}
