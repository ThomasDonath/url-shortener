= Settings

Initialize server parameters.

**settings.go**: Provide and initialize parameters as public variables from OS environment.

**init-xxx.go** provide a mechanism to get parameters from build tags: each function in Golang definied special package "init" will be called automatically during app initializing. _init-testauth.go_ will be called only if build tag is set - special syntax there: "// +build testauth". So I provide the array settings.go:var options depending on build tags.
