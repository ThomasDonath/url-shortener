package settings

import (
	"fmt"
	"os"
	"strconv"
	"url-shortener/logger"
)

var DbHostName string
var DbName string
var DbUserName string
var DbPasswd string
var DebugData bool
var AadTenant string
var AadClient string
var AadScope string
var DebugFullData bool
var TestAuthUserName string
var TestAuthGrantedRole int
var BrowserTitle string
var TestAuth bool = false

var UrlBufferLimit int = 10000
var IpBufferLimit = 10000

var GeoIpUseApi bool
var GeoDbPath string
var GeoApiAccount string
var GeoApiLicenseKey string
var GeoIpLite bool

var options []string

func getParm(envName string, dflt string, mandatory bool) string {

	val := os.Getenv(envName)
	if val == "" {
		if dflt == "" && mandatory {
			logger.Log.Panic(fmt.Sprintf("unable to get setting for %s", envName))
		} else {
			val = dflt
		}
	}
	if envName == "SHORTY_DB_PWD" || envName == "SHORTY_GeoLite2_TOKEN" {
		logger.Log.Debug(fmt.Sprintf("set %s to %v", envName, "***"))
	} else {
		logger.Log.Debug(fmt.Sprintf("set %s to %v", envName, val))
	}
	return val
}

func getParmB(envName string, dflt string) bool {

	var val bool
	var err error

	stringVal := os.Getenv(envName)
	if stringVal == "" {
		if dflt == "" {
			logger.Log.Panic(fmt.Sprintf("unable to get setting for %s", envName))
		} else {
			val, err = strconv.ParseBool(dflt)
		}
	} else {
		val, err = strconv.ParseBool(stringVal)
	}
	if err != nil {
		logger.Log.Panic(fmt.Sprintf("unable to get setting for %s because of %+v", envName, err))
	}
	if envName == "SHORTY_DB_PWD" || envName == "SHORTY_GeoLite2_TOKEN" {
		logger.Log.Debug(fmt.Sprintf("set %s to %v", envName, "***"))
	} else {
		logger.Log.Debug(fmt.Sprintf("set %s to %v", envName, val))
	}
	return val
}

func InitializeSettings() {
	// have to be set erlier in package logger: SHORTY_LOGGER, SHORTY_LOG_LEVEL
	DbHostName = getParm("SHORTY_DB_HOSTNAME", "", true)
	DbName = getParm("SHORTY_DB_NAME", "shorty", true)
	DbUserName = getParm("SHORTY_DB_USERNAME", "shorty", true)
	DbPasswd = getParm("SHORTY_DB_PWD", "", true)
	AadTenant = getParm("SHORTY_AAD_TENANT", "", true)
	AadClient = getParm("SHORTY_AAD_CLIENT_ID", "", true)
	AadScope = getParm("SHORTY_AAD_SCOPE", "", true)
	DebugData = getParmB("SHORTY_DEBUG_DATA", "false")
	DebugFullData = getParmB("SHORTY_DEBUG_FULL_DATA", "false")
	TestAuthUserName = getParm("SHORTY_TA_USERNAME", "no one", true)
	TestAuthGrantedRole, _ = strconv.Atoi(getParm("SHORTY_TA_ROLE", "0", true))
	BrowserTitle = getParm("SHORTY_BROWSER_TITLE", "shorty", true)

	GeoDbPath = getParm("SHORTY_GEO_DB_PATH", "/app/", false)
	GeoApiAccount = getParm("SHORTY_GeoIp_ACCOUNT", "", false)
	GeoApiLicenseKey = getParm("SHORTY_GeoIp_TOKEN", "", false)
	GeoIpLite = getParmB("SHORTY_GeoLite", "false")
	GeoIpUseApi = getParmB("SHORTY_GeoIp_API", "false")

	for _, o := range options {
		switch o {
		case "test-auth":
			logger.Log.Warn("Option test-auth will be compiled")
			TestAuth = true
		}
	}
}
