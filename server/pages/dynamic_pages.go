package pages

import (
	"bytes"
	"fmt"
	"text/template"
	"url-shortener/logger"
)

type templateVarsType struct {
	Title         string
	RequestedPath string
	Reason        string
	CssFile       string
}

var CssFileName string

// loaded from PageNoResult.html by load-pages.go
var pageTemplateNoResult *template.Template

// helper
func renderTemplate(template *template.Template, data any) string {
	buffer := new(bytes.Buffer)

	err := template.Execute(buffer, data)
	if err != nil {
		logger.Log.Fatal(fmt.Sprintf("unable to execute page template noResult. err: %+v", err))
	}

	return buffer.String()
}

// error page: alias does exists as "private" one and user is not authenticated
func PageNotVisible(requestedPath string) string {
	return renderTemplate(
		pageTemplateNoResult,
		templateVarsType{
			"not visible",
			requestedPath,
			`To use private aliases you have to <a href="/client/login">log in.</a>`,
			CssFileName,
		})
}

// error page: alias does NOT exist
func PageNotFound(requestedPath string) string {
	return renderTemplate(
		pageTemplateNoResult,
		templateVarsType{
			"not found",
			requestedPath,
			"I don't know such an alias.",
			CssFileName,
		})
}

// error page: Forbidden
func PageForbidden(requestedPath string) string {
	return renderTemplate(
		pageTemplateNoResult,
		templateVarsType{
			"forbidden",
			requestedPath,
			"You are not allowed to use this page.",
			CssFileName,
		})
}
