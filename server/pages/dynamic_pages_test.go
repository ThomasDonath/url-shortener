package pages

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"url-shortener/logger"
)

func TestLoadPages(t *testing.T) {
	logger.InitializeZapCustomLogger()

	LoadPages(".", "dist")
}

func TestPageNotVisible(t *testing.T) {
	logger.InitializeZapCustomLogger()
	LoadPages(".", "dist")

	p := PageNotVisible("noPath")

	assert.Contains(t, p, "I'm sorry - unfortunately I'm unable to serve your request")
	assert.Contains(t, p, "noPath")
	assert.Contains(t, p, "To use private aliases")
}

func TestPageNotFound(t *testing.T) {
	logger.InitializeZapCustomLogger()
	LoadPages(".", "dist")

	p := PageNotFound("noPath")

	assert.Contains(t, p, "I'm sorry - unfortunately I'm unable to serve your request")
	assert.Contains(t, p, "noPath")
	assert.Contains(t, p, "I don't know such an alias")
}

func TestPageForbidden(t *testing.T) {
	logger.InitializeZapCustomLogger()
	LoadPages(".", "dist")

	p := PageForbidden("noPath")

	assert.Contains(t, p, "I'm sorry - unfortunately I'm unable to serve your request")
	assert.Contains(t, p, "noPath")
	assert.Contains(t, p, "allowed")
}