package pages

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"url-shortener/logger"

	"go.uber.org/zap"
)

// load pages (static pages or templates from local filesystem) and prepare (parse) templates
func LoadPages(dirHtml string, dirCss string) {
	content, err := os.ReadFile(dirHtml + "/PageNoResult.html")
	if err != nil {
		logger.Log.Panic("unable to load PageNoResult", zap.String("error", err.Error()))
	}

	filepath.Walk(dirCss, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			if matched, err := filepath.Match("styles-*.css", filepath.Base(path)); err != nil {
				logger.Log.Panic("error looking for stylesheet", zap.String("error", err.Error()))
			} else if matched {
				CssFileName = filepath.Base(path)
			}
		}
		//logger.Log.Debug(fmt.Sprintf("searching for css %s, match %s", path, CssFileName))
		return nil
	})
	if CssFileName == "" {
		logger.Log.Panic("cant find stylesheet")
	}

	pageTemplateNoResult, err = template.New("noResult").Parse(strings.ReplaceAll(string(content), "\n", ""))
	if err != nil {
		logger.Log.Fatal(fmt.Sprintf("unable to parse page template noResult. err: %+v", err))
	}
}
