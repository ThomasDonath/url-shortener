= Pages

This package delivers all HTML pages.

== spa.go

Deliver the packaged SPA files from an in-memory filesystem

== static pages

We have some static pages ("not found", "not visible").

They all be rendered from PagesNoResult.html 

* used as Go-template
* loaded during startup (load-pages.go)

Rendering implements dynamic_pages.go as a dedicated func per target page.
