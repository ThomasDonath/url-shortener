package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"url-shortener/def"
	"url-shortener/handlers"
	"url-shortener/ip_statistics"
	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/settings"
	"url-shortener/store"
)

var shutdownInProgress bool = false

func main() {
	logger.InitializeZapCustomLogger()
	logger.Log.Info("intializing...")
	settings.InitializeSettings()
	pages.LoadPages("pages", "pages")

	ip_statistics.Init(settings.GeoDbPath)
	defer ip_statistics.Shutdown()

	logger.Log.Info("Starting server")

	///////////////////////////////////////////////////////////////////////////////////
	// Handlers
	http.Handle(def.UrlClientFiles, http.StripPrefix(def.UrlClientFiles, http.FileServer(http.Dir("./pages/dist/"))))
	http.HandleFunc(def.UrlClientApp, handlers.Client)
	http.HandleFunc(def.UrlClientConfig, handlers.HandleClientConfig)

	http.HandleFunc("/", handlers.MwProtector(handlers.HandleRoot, true))
	// API methodes
	http.HandleFunc(def.UrlApiAlias, handlers.MwProtector(handlers.HandleApiAlias, false))
	http.HandleFunc(def.UrlApiAliasStats, handlers.MwProtector(handlers.HandleApiAliasStats, false))

	http.HandleFunc(def.UrlHealthCheck, HandleHealthCheck)

	///////////////////////////////////////////////////////////////////////////////////
	if err := store.InitStore(settings.DebugData, settings.DebugFullData, true); err != nil {
		log.Fatalf("Error intializing store db:%+v", err)
	}

	// prepare for Signals
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)
	doTerminate := make(chan bool, 1)
	go func() {
		sig := <-signalChannel
		fmt.Println()
		fmt.Println(sig)
		doTerminate <- true
	}()

	// start server
	srv := &http.Server{
		Addr:    ":8080",
		Handler: nil,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Error starting server:%+v", err)
		}
	}()
	logger.Log.Info("Server started")

	// clear URL buffer every hour
	go store.BufferClear(3600)

	// clear IP buffer every day
	go ip_statistics.ClearIpCache(3600 * 24)

	// handle shutdown
	<-doTerminate
	logger.Log.Info("Terminating")
	shutdownInProgress = true
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	store.DbClose()

	defer func() {
		// extra handling here
		cancel()
	}()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server shutdown failed:%+v", err)
	}
	logger.Log.Info("Server terminated")
}

func HandleHealthCheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if !shutdownInProgress {
		if r.Method == http.MethodGet {
			if store.DbPing() {
				if store.DbPing() {
				w.WriteHeader(http.StatusOK)
					w.Write([]byte("healthy"))
			} else {
				w.WriteHeader(http.StatusServiceUnavailable)
				w.Write([]byte("service not healthy"))
			}
			} else {
				w.WriteHeader(http.StatusServiceUnavailable)
				w.Write([]byte("service not healthy"))
			}
		} else {
			logger.Log.Error("call to health check with method " + r.Method)
			w.WriteHeader((http.StatusMethodNotAllowed))
		}
	} else {
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte("shutdown in progess"))
	}
}
