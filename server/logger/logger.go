package logger

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	// Log variable is a globally accessible variable which will be initialized when the InitializeZapCustomLogger function is executed successfully.
	Log *zap.Logger
)


// InitializeZapCustomLogger Funtion initializes a logger using uber-go/zap package in the application.
// config will be read from OS environment
func InitializeZapCustomLogger() {
	configEncoding := os.Getenv("SHORTY_LOGGER")
	if configEncoding == "" {
		configEncoding = "console"
	}
	configLevelEnv := os.Getenv("SHORTY_LOG_LEVEL")
	configLevel := zap.NewAtomicLevelAt(zapcore.InfoLevel)
	if configLevelEnv == "DEBUG" {
		configLevel = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	}

	conf := zap.Config{
		Encoding:    configEncoding,
		Level:       configLevel,
		OutputPaths: []string{"stdout"},
		EncoderConfig: zapcore.EncoderConfig{
			LevelKey:     "level",
			TimeKey:      "time",
			CallerKey:    "file",
			MessageKey:   "msg",
			EncodeLevel:  zapcore.LowercaseLevelEncoder,
			EncodeTime:   zapcore.ISO8601TimeEncoder,
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}
	configuredLogger, err := conf.Build()
	if err != nil {
		panic("unable to initialize logger")
	}
	Log = configuredLogger
}
