#!/bin/bash
#
# start shorty from on a machine 
# MySQL database must be running, if not on localhost then change SHORTY_DB_HOSTNAME
# must be available on port 3306 (default)

export SHORTY_LOG_LEVEL="INFO"
export SHORTY_DB_HOSTNAME=localhost
export SHORTY_GeoIp_API=true
export SHORTY_GeoLite=true
export SHORTY_GEO_DB_PATH="./"
export SHORTY_AAD_SCOPE="api://shorty.dev.thomasdonathgematik.onmicrosoft.com/access"
export SHORTY_BROWSER_TITLE="Shorty!"

# also set these sensible data
# export SHORTY_DB_PWD=
# export SHORTY_GeoIp_ACCOUNT=
# export SHORTY_GeoIp_TOKEN=
# export SHORTY_DB_PWD=
# export SHORTY_AAD_TENANT=
# export SHORTY_AAD_CLIENT_ID=

./binaries/url-shortener_darwin_arm64/url-shortener
