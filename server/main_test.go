package main

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"url-shortener/def"
	"url-shortener/handlers"
	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/settings"
	"url-shortener/store"
)

func TestMain(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)
	pages.LoadPages("pages", "pages/dist")

	assert.HTTPStatusCode(t, handlers.Client, http.MethodGet, def.UrlClientApp, nil, http.StatusOK, "access client")
	assert.HTTPStatusCode(t, handlers.HandleClientConfig, http.MethodGet, def.UrlClientConfig, nil, http.StatusOK, "access client config")
	assert.HTTPStatusCode(t, handlers.MwProtector(handlers.HandleRoot, true), http.MethodGet, "/", nil, http.StatusOK, "access root")
	assert.HTTPStatusCode(t, handlers.MwProtector(handlers.HandleApiAlias, false), http.MethodGet, def.UrlApiAlias, nil, http.StatusForbidden, "access API")
	assert.HTTPStatusCode(t, handlers.MwProtector(handlers.HandleApiAliasStats, false), http.MethodGet, def.UrlApiAliasStats, nil, http.StatusInternalServerError, "access stats")
	assert.HTTPStatusCode(t, HandleHealthCheck, http.MethodGet, def.UrlHealthCheck, nil, http.StatusOK, "access health check")

}

func TestHandleHealthCheck(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)
	pages.LoadPages("pages", "pages/dist")

	assert.HTTPStatusCode(t, HandleHealthCheck, http.MethodGet, def.UrlHealthCheck, nil, http.StatusOK, "Healtcheck is ok")
	assert.HTTPStatusCode(t, HandleHealthCheck, http.MethodPost, def.UrlHealthCheck, nil, http.StatusMethodNotAllowed, "Healthcheck GET only")

	shutdownInProgress = true
	assert.HTTPStatusCode(t, HandleHealthCheck, http.MethodGet, def.UrlHealthCheck, nil, http.StatusServiceUnavailable, "Healtcheck is not ok")
}
