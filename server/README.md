= Server implementation

== Buffer

We have a buffer in memory (store/store.go:var buffer) to response to already known aliases as fast as possible. If a requested alias is not in buffer it will be added after read from database.

To get deletes or changes from other server- (container-) instances synchronized the buffer will be cleared every one hour.
