package handlers

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/settings"
)

func TestClientConfig(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()

	assert.HTTPStatusCode(t, HandleClientConfig, http.MethodGet, def.UrlClientConfig, nil, http.StatusOK)

	req, err := http.NewRequest(http.MethodGet, def.UrlClientConfig, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	var handler http.HandlerFunc = HandleClientConfig
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Result().StatusCode)
	assert.JSONEq(t, fmt.Sprintf(`{"browserTitle":"shorty", "tenantId": "%s", "scope": "%s", "clientId": "%s"}`, settings.AadTenant, settings.AadScope, settings.AadClient), rr.Body.String())

	oldTenant := settings.AadTenant
	settings.AadTenant = ""
	assert.HTTPStatusCode(t, HandleClientConfig, http.MethodGet, def.UrlClientConfig, nil, http.StatusInternalServerError)
	settings.AadTenant = oldTenant

	oldClientId := settings.AadTenant
	settings.AadTenant = ""
	assert.HTTPStatusCode(t, HandleClientConfig, http.MethodGet, def.UrlClientConfig, nil, http.StatusInternalServerError)
	settings.AadClient = oldClientId
}
