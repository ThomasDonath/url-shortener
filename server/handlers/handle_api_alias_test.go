package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/settings"
	"url-shortener/store"
	"url-shortener/userContext"
)

func testerMW(next http.HandlerFunc, useRole int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := userContext.NewUserContext(r.Context(), settings.TestAuthUserName, useRole)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
}

func checkJsonResponse(t *testing.T, url string, role int, results any) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := testerMW(HandleApiAlias, role)
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode)

	if !json.Valid(rr.Body.Bytes()) {
		logger.Log.Error(fmt.Sprintf("got invalid json response %+v", rr.Body.String()))
	}
	err = json.Unmarshal(rr.Body.Bytes(), results)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error unmarshalling response %+v", err))
	}
	//logger.Log.Debug(fmt.Sprintf("body %+v unmarshalled json %+v", rr.Body.String(), results))
}

func TestHandleApiAlias(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)

	// handle missed query parameters
	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), http.MethodGet, def.UrlApiAlias, nil, http.StatusOK) // "empty parameters are ok?")

	// handle missed body
	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), http.MethodPost, def.UrlApiAlias, nil, http.StatusBadRequest)
	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), http.MethodPut, def.UrlApiAlias, nil, http.StatusBadRequest)

	// test GetAll()
	{
		testThis := func(role int) {
			var results def.ApiAliasesReturnType
			assert.HTTPSuccess(t, testerMW(HandleApiAlias, role), http.MethodGet, def.UrlApiAlias, nil)
			checkJsonResponse(t, def.UrlApiAlias, role, &results)
			assert.Equal(t, 3, len(results.Data))
			assert.Equal(t, uint(3), results.RowCount)
		}
		testThis(def.ROLES["Viewer"])
		testThis(def.ROLES["Editor"])
		testThis(def.ROLES["Administrator"])

		assert.HTTPError(t, testerMW(HandleApiAlias, def.ROLES["Pulic"]), http.MethodGet, def.UrlApiAlias, nil)
	}

	///////////////////////////////////////////////////////////////////////////
	{ // test for *named* alias
		testThis := func(payload []byte, role int) {
			{
				req, err := http.NewRequest(http.MethodPost, def.UrlApiAlias, bytes.NewBuffer(payload))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := testerMW(HandleApiAlias, role)
				handler.ServeHTTP(rr, req)

				assert.Equal(t, http.StatusCreated, rr.Result().StatusCode)

				var retv def.ApiReturnType
				err = json.Unmarshal(rr.Body.Bytes(), &retv)
				if err != nil {
					panic(fmt.Sprintf("error unmarshalling json %+v", err))
				}
				assert.Equal(t, def.ErrorCodeOk, retv.StatusCode)
			}
			{ // a secound call should raise "alias already in use"
				req, err := http.NewRequest(http.MethodPost, def.UrlApiAlias, bytes.NewBuffer(payload))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := testerMW(HandleApiAlias, role)
				handler.ServeHTTP(rr, req)

				assert.Equal(t, http.StatusCreated, rr.Result().StatusCode)

				var retv def.ApiReturnType
				err = json.Unmarshal(rr.Body.Bytes(), &retv)
				if err != nil {
					panic(fmt.Sprintf("error unmarshalling json %+v", err))
				}
				assert.Equal(t, def.ErrorCodeAliasInUse, retv.StatusCode)
			}
		}
		payload, _ := json.Marshal(def.NewUrlEntryType("https://new1.com/index.html", "new1", false, settings.TestAuthUserName, "", "descriptiontext"))
		testThis(payload, def.ROLES["Editor"])
		payload, _ = json.Marshal(def.NewUrlEntryType("https://new1.com/index.html", "new2", false, settings.TestAuthUserName, "", "descriptiontext"))
		testThis(payload, def.ROLES["Administrator"])
	}

	{ // create a random alias (insert)
		testThis := func(payload []byte, role int) {
			req, err := http.NewRequest(http.MethodPost, def.UrlApiAlias, bytes.NewBuffer(payload))
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			handler := testerMW(HandleApiAlias, role)
			handler.ServeHTTP(rr, req)

			assert.Equal(t, http.StatusCreated, rr.Result().StatusCode)
			assert.NotEmpty(t, rr.Body)
			// Response have to be JSON of ApiReturnType, generated alias has to be in Data.Alias
			var result def.ApiReturnType
			err = json.Unmarshal(rr.Body.Bytes(), &result)
			assert.NoError(t, err)
			assert.NotEmpty(t, result.Data.Alias)
		}
		b, _ := json.Marshal(def.NewUrlEntryType("https://new1.com/index.html", "", false, settings.TestAuthUserName, "", "descriptiontext"))
		testThis(b, def.ROLES["Editor"])
		testThis(b, def.ROLES["Administrator"])
	}

	///////////////////////////////////////////////////////////////////////////
	{ // test for named alias (insert + update)
		b, _ := json.Marshal(def.NewUrlEntryType("https://new1.com/index.html", "new1", false, settings.TestAuthUserName, "", "descriptiontext22"))

		ireq, ierr := http.NewRequest(http.MethodPost, def.UrlApiAlias, bytes.NewBuffer(b))
		if ierr != nil {
			t.Fatal(ierr)
		}
		irr := httptest.NewRecorder()
		handler := testerMW(HandleApiAlias, 6)
		handler.ServeHTTP(irr, ireq)
		assert.Equal(t, http.StatusCreated, irr.Result().StatusCode)

		e, qerr := store.DbFindUrlEntry("new1")
		if qerr != nil {
			logger.Log.Panic("unable to find inserted test row")
		}
		e.Public = !e.Public
		ub, _ := json.Marshal(e)

		ureq, uerr := http.NewRequest(http.MethodPut, def.UrlApiAlias, bytes.NewBuffer(ub))
		if uerr != nil {
			t.Fatal(uerr)
		}
		urr := httptest.NewRecorder()
		handler.ServeHTTP(urr, ureq)
		assert.Equal(t, http.StatusOK, urr.Result().StatusCode)

		// no error if update with the unchanged row
		u2req, u2err := http.NewRequest(http.MethodPut, def.UrlApiAlias, bytes.NewBuffer(ub))
		if u2err != nil {
			t.Fatal(u2err)
		}
		u2rr := httptest.NewRecorder()
		handler.ServeHTTP(u2rr, u2req)
		assert.Equal(t, http.StatusOK, u2rr.Result().StatusCode)
	}

	///////////////////////////////////////////////////////////////////////////
	// DEL ohne ID
	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), http.MethodDelete, def.UrlApiAlias, nil, http.StatusBadRequest)

	_, idGat, _ := store.FindUrl4Alias("gat", def.ROLES["God"])
	_, idGit, _ := store.FindUrl4Alias("gat", def.ROLES["God"])
	// foreign entry w/o Administrator do nothing
	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), "DELETE", def.UrlApiAlias+idGat.String(), nil, http.StatusAccepted)
	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), "DELETE", def.UrlApiAlias+idGat.String(), nil, http.StatusAccepted)
	// should be deleted
	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), "DELETE", def.UrlApiAlias+idGit.String(), nil, http.StatusAccepted)

	assert.HTTPStatusCode(t, testerMW(HandleApiAlias, 6), "PATCH", def.UrlApiAlias, nil, http.StatusMethodNotAllowed)
}
