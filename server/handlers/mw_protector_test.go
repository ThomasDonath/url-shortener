package handlers

import (
	"net/http"
	"testing"
	
	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/settings"
	"url-shortener/store"

	"github.com/stretchr/testify/assert"
)

func TestMwProtector(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)
	pages.LoadPages("../pages", "../pages")

	assert.HTTPStatusCode(t, MwProtector(HandleApiAlias, false), "GET", def.UrlApiAlias, nil, http.StatusForbidden)
	assert.HTTPStatusCode(t, MwProtector(HandleApiAliasStats, false), "GET", def.UrlApiAliasStats, nil, http.StatusInternalServerError)
}