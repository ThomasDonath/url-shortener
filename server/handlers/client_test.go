package handlers

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/settings"
	"url-shortener/store"

	"github.com/stretchr/testify/assert"
)

func TestClient(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)
	pages.LoadPages("../pages", "../pages/dist")

	req, err := http.NewRequest(http.MethodGet, def.UrlClientApp, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	Client(rr, req)

	b, _ := io.ReadAll(rr.Result().Body)

	assert.Contains(t, string(b), `<base href="/clientfiles/">`, "index.html")
}