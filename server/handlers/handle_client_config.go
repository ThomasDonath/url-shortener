package handlers

import (
	"fmt"
	"net/http"

	"url-shortener/logger"
	"url-shortener/settings"
)

func HandleClientConfig(w http.ResponseWriter, r *http.Request) {
	if settings.AadTenant == "" || settings.AadClient == "" || settings.AadScope == "" {
		logger.Log.Error(fmt.Sprintf("unable to get AAD config. tenant=%s, clientId=%s, scope=%s", settings.AadTenant, settings.AadClient, settings.AadScope))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("can't find AAD config"))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Header().Add("Content-Type", "application/json; charset=utf-8")
		w.Write([]byte(
			fmt.Sprintf(`{"tenantId": "%s", "clientId": "%s", "scope": "%s", "browserTitle": "%s"}`,
				settings.AadTenant, settings.AadClient, settings.AadScope, settings.BrowserTitle)))
	}
}
