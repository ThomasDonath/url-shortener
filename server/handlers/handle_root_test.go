package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"url-shortener/ip_statistics"
	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/settings"
	"url-shortener/store"
)

func checkRedirect(t *testing.T, url string, redirectedTo string, code int) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := MwProtector(HandleRoot, false)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, code, rr.Result().StatusCode, "tested "+url)
	// no way to check the redirect target as it will be handled with http.redirect inside the handler
}

func TestMwRouter(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)
	ip_statistics.Init(settings.GeoDbPath)
	pages.LoadPages("../pages", "../pages/dist")

	assert.HTTPSuccess(t, MwProtector(HandleRoot, false), "GET", "/", nil)
	assert.HTTPBodyContains(t, MwProtector(HandleRoot, false), "GET", "/", nil, "<app-root></app-root>")

	checkRedirect(t, "/gat", "https://gitlab.com/viewer", http.StatusTemporaryRedirect)
	checkRedirect(t, "/GAT/", "https://gitlab.com/viewer", http.StatusTemporaryRedirect) // and test if searched with lowercase
	checkRedirect(t, "/nono", "", http.StatusNotFound)
	checkRedirect(t, "/nono/", "", http.StatusNotFound)
	checkRedirect(t, "/no<no/", "", http.StatusBadRequest)

	assert.HTTPStatusCode(t, MwProtector(HandleRoot, false), http.MethodGet, "/git", nil, http.StatusForbidden, "private entry should not be visible w/o login")
}
