package handlers

import (
	"fmt"
	"net/http"
	"strings"

	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/statistics_writer"
	"url-shortener/store"
	"url-shortener/userContext"
)

// handle all calls for "/" - includes forwarding for a requested alias
func HandleRoot(w http.ResponseWriter, r *http.Request) {
	// these files will searched for browsers in root and we redirect them to ./dist/
	if r.URL.Path == "/favicon.ico" {
		w.Header().Set("Content-Type", "image/x-icon")
		rawFile, _ := pages.AngularFiles.ReadFile("dist/favicon.ico")
		w.Write(rawFile)
	} else if r.URL.Path == "/" {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		setHeaders(w)

		rawFile, _ := pages.AngularFiles.ReadFile("dist/index.html")
		w.Write(rawFile)
	} else if r.URL.Path == "/apple-touch-icon.png" { // will be requested by iOS browser, I don't nor have this, but forward it
		w.Header().Set("Content-Type", "image/x-icon")
		rawFile, _ := pages.AngularFiles.ReadFile("dist/apple-touch-icon.png")
		w.Write(rawFile)
	} else if r.URL.Path == "/apple-touch-icon-precomposed.png" { // will be requested by iOS browser, I don't nor have this, but forward it
		w.Header().Set("Content-Type", "image/x-icon")
		rawFile, _ := pages.AngularFiles.ReadFile("dist/apple-touch-icon-precomposed.png")
		w.Write(rawFile)
	} else {
		///////////////////////////////////////////////////////
		// get URL for requested alias and redirect to this URL
		logger.Log.Debug("handleRoot() entry for public URL: " + r.URL.Path)

		userCtx, userFound := userContext.FromUserContext(r.Context())
		if !userFound {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("server error: no user context found"))
			return
		}
		loggedInUser := userCtx.UserName
		loggedInRole := userCtx.GrantedRole

		logger.Log.Debug(fmt.Sprintf("handleRoot() User %s, Role %d ", loggedInUser, loggedInRole))

		var requestUrl = strings.ToLower(r.URL.Path[1:len(r.URL.Path)]) // remove leading "/" and force lowercase
		if strings.Contains(requestUrl, "<") {
			w.WriteHeader(http.StatusBadRequest)
			logger.Log.Error("malicous hostname for redirect " + requestUrl)
			return
		}
		if requestUrl[len(requestUrl)-1:] == "/" { // remove trailing "/"
			requestUrl = requestUrl[0 : len(requestUrl)-1]
		}

		fullUrl, id, err := store.FindUrl4Alias(requestUrl, loggedInRole)

		logger.Log.Debug(fmt.Sprintf("HandleRoot(): found URL: %s, error: %+v", fullUrl, err))
		// redirecting
		if fullUrl != "" {
			http.Redirect(w, r, fullUrl, http.StatusTemporaryRedirect)
			go statistics_writer.WriteStatistics(r.Context(), r, id, "success", loggedInRole)
		} else {
			if err.Error() == store.ERROR_NOT_VISIBLE {
				go statistics_writer.WriteStatistics(r.Context(), r, id, "not visible", loggedInRole)
				w.WriteHeader(http.StatusForbidden)
				logger.Log.Warn(fmt.Sprintf("Alias not visible for redirect %s", requestUrl))
				w.Write([]byte(pages.PageNotVisible(requestUrl)))
			} else {
				w.WriteHeader(http.StatusNotFound)
				logger.Log.Error(fmt.Sprintf("Alias not found for redirect %s", requestUrl))
				w.Write([]byte(pages.PageNotFound(requestUrl)))
			}
			logger.Log.Debug(fmt.Sprintf("handleRoot(): return %d for %s", http.StatusNotFound, requestUrl))
		}
		return
	}
}
