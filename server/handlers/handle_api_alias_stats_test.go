package handlers

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/settings"
	"url-shortener/store"
	"url-shortener/userContext"
)

func TestHandleApiAliasStats(t *testing.T) {
	logger.InitializeZapCustomLogger()
	settings.InitializeSettings()
	store.InitStore(true, false, true)
	pages.LoadPages("../pages", "../pages/dist")

	_, gitId, err := store.FindUrl4Alias("git", def.ROLES["God"])
	if err != nil {
		panic(fmt.Sprintf("cant find alias to test with, err %+v", err))
	}

	{ // test for empty context (no user info)
		req, err := http.NewRequest(http.MethodGet, def.UrlApiAliasStats, nil)
		if err != nil {
			t.Fatal(err)
		}
		resp := httptest.NewRecorder()
		HandleApiAliasStats(resp, req)

		assert.Equal(t, http.StatusInternalServerError, resp.Result().StatusCode)
	}
	
	{ // Forbidden
		ctx := userContext.NewUserContext(context.Background(), "anonymous", def.ROLES["Public"])
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, def.UrlApiAliasStats, nil)
		if err != nil {
			t.Fatal(err)
		}
		q := req.URL.Query()
		q.Add("AliasId", gitId.String())
		q.Add("format", "day")
		q.Add("sortBy", "1")
		q.Add("sortOrder", "asc")
		q.Add("pageNumber", `"0"`)
		q.Add("pageSize", `"10"`)
		req.URL.RawQuery = q.Encode()

		resp := httptest.NewRecorder()
		HandleApiAliasStats(resp, req)

		assert.Equal(t, http.StatusForbidden, resp.Result().StatusCode)
	}

	{ // Forbidden for Viewers
		ctx := userContext.NewUserContext(context.Background(), "anonymous", def.ROLES["Viewer"])
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, def.UrlApiAliasStats, nil)
		if err != nil {
			t.Fatal(err)
		}
		q := req.URL.Query()
		q.Add("AliasId", gitId.String())
		q.Add("format", "day")
		q.Add("sortBy", "1")
		q.Add("sortOrder", "asc")
		q.Add("pageNumber", `"0"`)
		q.Add("pageSize", `"10"`)
		req.URL.RawQuery = q.Encode()

		resp := httptest.NewRecorder()
		HandleApiAliasStats(resp, req)

		assert.Equal(t, http.StatusForbidden, resp.Result().StatusCode)
	}

	{ // Success
		ctx := userContext.NewUserContext(context.Background(), "anonymous", def.ROLES["Editor"])
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, def.UrlApiAliasStats, nil)
		if err != nil {
			t.Fatal(err)
		}
		q := req.URL.Query()
		q.Add("AliasId", gitId.String())
		q.Add("format", "day")
		q.Add("sortBy", "1")
		q.Add("sortOrder", "asc")
		q.Add("pageNumber", `"0"`)
		q.Add("pageSize", `"10"`)
		req.URL.RawQuery = q.Encode()
		
		resp := httptest.NewRecorder()
		HandleApiAliasStats(resp, req)

		assert.Equal(t, http.StatusOK, resp.Result().StatusCode)
	}
}
