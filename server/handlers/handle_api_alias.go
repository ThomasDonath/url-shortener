package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/store"
	"url-shortener/userContext"

	"github.com/google/uuid"
)

// generate a HTTP response from a struct eg. def.ApiReturnType
func struct2response(statusCode int, v any, w http.ResponseWriter) {
	resultJson, err := json.Marshal(v)

	if err != nil {
		logger.Log.Error(fmt.Sprintf("error transforming result to json %+v", err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Add("Content-Type", "text/plain; charset=utf-8")
		w.Write([]byte(fmt.Sprintf("server error %+v", err)))
	} else {
		w.WriteHeader(statusCode)
		w.Header().Add("Content-Type", "application/json; charset=utf-8")
		w.Write(resultJson)
	}
}

// handle all calls for the RESTful API
func HandleApiAlias(w http.ResponseWriter, r *http.Request) {
	logger.Log.Debug(fmt.Sprintf("called Alias api with %s on %s", r.Method, r.URL.Path))

	// after base path should be only the idString argument followed by optionally query params, which are not in r.URL.Path
	idString := strings.Split(r.URL.Path[len((def.UrlApiAlias)):len(r.URL.Path)], "/")[0]

	// user, role from JWT
	userCtx, userFound := userContext.FromUserContext(r.Context())
	if !userFound {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("server error: no user context found"))
	}

	switch r.Method {
	case http.MethodGet:
		params, err := url.ParseQuery(r.URL.RawQuery)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("server error getting parameters %+v", err)))
		}

		paramFilterUrl := params.Get("filterUrl")
		paramFilterAlias := params.Get("filterAlias")
		paramFilterOwner := params.Get("filterOwner")
		paramSortColumn := params.Get("sortColumn")
		paramSortOrder := params.Get("sortOrder")
		paramPageNumber := params.Get("pageNumber")
		pageNumber, _ := strconv.ParseUint(paramPageNumber, 10, 0)
		paramPageSize := params.Get("pageSize")
		pageSize, _ := strconv.ParseUint(paramPageSize, 10, 0)

		var retv def.ApiAliasesReturnType
		retv.Data, retv.RowCount, err = store.GetEntries(paramFilterUrl, paramFilterAlias, paramFilterOwner, paramSortColumn, paramSortOrder, pageNumber, pageSize, userCtx.GrantedRole)
		if err != nil {
			if err.Error() == "user is not allowed to query entries" {
				w.WriteHeader(http.StatusForbidden)
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("server error %+v", err)))
			}
		} else {
			struct2response(http.StatusOK, retv, w)
		}
	case http.MethodPost:
		var entry def.UrlEntryType

		if r.Body == nil {
			logger.Log.Error("POST without body")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("missing body"))
		} else if err := json.NewDecoder(r.Body).Decode(&entry); err != nil {
			logger.Log.Error(fmt.Sprintf("POST error unmarshalling JSON: %+v", entry))
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("error %+v", err)))
		} else { // switch Random/Named Alias, give error back
			if entry.Alias == "" {
				retv, err := store.CreateRandomEntry(entry, userCtx.GrantedRole)
				if err != nil {
					logger.Log.Error(fmt.Sprintf("create random entry error: %+v", err))
					if err.Error() == "user is not allowed to create entries" {
						w.WriteHeader(http.StatusForbidden)
					} else {
						w.WriteHeader(http.StatusInternalServerError)
						w.Write([]byte(fmt.Sprintf("server error %+v", err)))
					}
				} else {
					logger.Log.Debug(fmt.Sprintf("create random entry success, result: %+v", retv))
					struct2response(http.StatusCreated, retv, w)
				}
			} else {
				retv, err := store.CreateNamedEntry(entry, userCtx.GrantedRole)
				if err != nil {
					logger.Log.Error(fmt.Sprintf("create named entry error: %+v", err))
					if err.Error() == "user is not allowed to create entries" {
						w.WriteHeader(http.StatusForbidden)
					} else {
						w.WriteHeader(http.StatusInternalServerError)
						w.Write([]byte(fmt.Sprintf("server error %+v", err)))
					}
				} else {
					logger.Log.Debug(fmt.Sprintf("create named entry success, result: %+v", retv))
					struct2response(http.StatusCreated, retv, w)
				}
			}
		}
	case http.MethodPut:
		var entry def.UrlEntryType

		if r.Body == nil {
			logger.Log.Error("PUT without body")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("missing body"))
		} else if err := json.NewDecoder(r.Body).Decode(&entry); err != nil {
			logger.Log.Error(fmt.Sprintf("PUT error unmarshalling JSON: %+v", entry))
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("error %+v", err)))
		} else {
			retv, err := store.UpdateEntry(entry, userCtx.GrantedRole, userCtx.UserName)

			if err != nil {
				logger.Log.Error(fmt.Sprintf("UpdateEntry Error: %+v", err))
				if err.Error() == "user is not allowed to update entries" {
					w.WriteHeader(http.StatusUnauthorized)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte(fmt.Sprintf("server error %v+", err)))
				}
			} else {
				logger.Log.Debug(fmt.Sprintf("UpdateEntry success, result: %v", retv))
				struct2response(http.StatusOK, retv, w)
			}
		}
	case http.MethodDelete:
		if idString == "" {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			id, errParse := uuid.Parse(idString)
			if errParse != nil {
				logger.Log.Error(fmt.Sprintf("delete: error parsing ID %+v", errParse))
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			_, err := store.DeleteEntry(id, userCtx.GrantedRole, userCtx.UserName)
			if err != nil {
				logger.Log.Error(fmt.Sprintf("api delete entry with error %v", err))

				if err.Error() == "user is not allowed to delete entries" {
					w.WriteHeader(http.StatusForbidden)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
				}
			} else {
				w.WriteHeader(http.StatusAccepted)
			}
		}
	case http.MethodOptions:
		w.Header().Add("Connection", "keep-alive")
		w.Header().Add("Access-Control-Allow-Methods", "POST, OPTIONS, GET, DELETE, PUT")
		w.Header().Add("Access-Control-Allow-Headers", "content-type")

	default:
		logger.Log.Error("Alias API called with wrong method " + r.Method)
		w.WriteHeader((http.StatusMethodNotAllowed))
	}
}
