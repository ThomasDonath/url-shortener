package handlers

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	"net/http"
	"strings"

	jwt "github.com/golang-jwt/jwt/v5"
	"github.com/lestrrat-go/jwx/v2/jwk"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/pages"
	"url-shortener/settings"
	"url-shortener/userContext"
)

// extract a JWT from Header (a call from Angular client) or cookie (a browser call for an alias)
// ignoreTokenFailure - true for browser calls with to beware users from error "token expired" for an old cookie
func MwProtector(next http.HandlerFunc, ignoreTokenFailure bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.Log.Debug(fmt.Sprintf("mwProtector() entry URL=%s", r.URL.Path))

		var tokenString string
		var isCookieToken = false

		if tokenHeader := r.Header.Get("Authorization"); len(tokenHeader) > 7 {
			tokenString = tokenHeader[7:]
			isCookieToken = false
		} else {
			if cookie, err := r.Cookie("auth"); err == nil {
				tokenString = cookie.Value
				isCookieToken = true
			}
		}

		if tokenString == "" {
			if settings.TestAuth {
				ctx := userContext.NewUserContext(r.Context(), settings.TestAuthUserName, settings.TestAuthGrantedRole)
				next.ServeHTTP(w, r.WithContext(ctx))
			} else {
				ctx := userContext.NewUserContext(r.Context(), "anonymous", def.ROLES["Public"])
				next.ServeHTTP(w, r.WithContext(ctx))
			}
		} else {
			token, err := verifyToken(r.Context(), tokenString, isCookieToken)
			if err != nil {
				if ignoreTokenFailure {
					ctx := userContext.NewUserContext(r.Context(), "anonymous", def.ROLES["Public"])
					next.ServeHTTP(w, r.WithContext(ctx))
				} else {
					logger.Log.Error(fmt.Sprintf("Auth error %+v", err))
					w.WriteHeader(http.StatusForbidden)
					w.Write([]byte(pages.PageForbidden(r.URL.Path)))
				}
			} else {
				var claims jwt.MapClaims
				//logger.Log.Debug(fmt.Sprintf("Token: %+v", token))
				claims, _ = token.Claims.(jwt.MapClaims)
				userName := fmt.Sprint(claims["preferred_username"])
				roleClaim := fmt.Sprint(claims["roles"]) // find the highest role from the array (there may be more than one!)

				var roleName string

				if strings.Contains(roleClaim, "Administrator") {
					roleName = "Administrator"
				} else if strings.Contains(roleClaim, "Editor") {
					roleName = "Editor"
				} else if strings.Contains(roleClaim, "Viewer") {
					roleName = "Viewer"
				} else {
					roleName = "Public"
				}

				ctx := userContext.NewUserContext(r.Context(), userName, def.ROLES[roleName])
				logger.Log.Debug(fmt.Sprintf("set context user %s, role name %s role %d", userName, roleName, def.ROLES[roleName]))
				next.ServeHTTP(w, r.WithContext(ctx))
			}
		}
	}
}

/*
verify the given token.

If "ignoreExp" is true we ignore the fact that the token is expired.
"req" will be used to pass the Context to subsequent calls.
*/
func verifyToken(ctx context.Context, tokenString string, ignoreExp bool) (*jwt.Token, error) {

	if tokenString == "" {
		return nil, errors.New("missing auth header")
	}

	keySet, err := jwk.Fetch(ctx, "https://login.microsoftonline.com/common/discovery/v2.0/keys")
	if err != nil {
		logger.Log.Error(fmt.Sprintf("error loading keys %+v", err))
		return nil, err
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		kid, ok := token.Header["kid"].(string)
		if !ok {
			return nil, fmt.Errorf("kid header not found")
		}

		keys, ok := keySet.LookupKeyID(kid)
		if !ok {
			return nil, fmt.Errorf("key %v not found", kid)
		}

		publickey := &rsa.PublicKey{}
		err = keys.Raw(publickey)
		if err != nil {
			return nil, fmt.Errorf("could not parse pubkey")
		}

		return publickey, nil
	})

	switch {
	case token.Valid:
		//fmt.Println("You look nice today")
	case errors.Is(err, jwt.ErrTokenMalformed):
		logger.Log.Error("maformed OAuth token")
	case errors.Is(err, jwt.ErrTokenSignatureInvalid):
		// Invalid signature
		logger.Log.Error("Invalid signature")
	case errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet):
		if ignoreExp {
			logger.Log.Debug("OAuth token expired but ignored")
			// FIXME shouldn't be older than 24h
			err = nil
		} else {
			logger.Log.Error("OAuth token expired")
		}
	default:
		logger.Log.Error("Couldn't handle this token.")
	}
	/*
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				logger.Log.Error(fmt.Sprintf("maformed OAuth token %+v", err))
			} else if ve.Errors&(jwt.ValidationErrorExpired) != 0 {
				if ignoreExp {
					logger.Log.Debug("OAuth token expired but ignored")
					// FIXME shouldn't be older than 24h
					err = nil
				} else {
					logger.Log.Error("OAuth token expired")
				}
			} else if ve.Errors&(jwt.ValidationErrorNotValidYet) != 0 {
				logger.Log.Error("OAuth token not valid yet")
			} else {
				logger.Log.Error(fmt.Sprintf("invalid OAuth token: %+v", err))
			}
		} */

	if err != nil {
		return nil, err
	}
	return token, nil
}
