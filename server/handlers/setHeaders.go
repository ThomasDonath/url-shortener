package handlers

import "net/http"

func setHeaders(w http.ResponseWriter) {
	w.Header().Set("Content-Security-Policy", "default-src 'none'; font-src 'self' https://fonts.gstatic.com/; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self' ; connect-src 'self' https://login.microsoftonline.com/ https://graph.microsoft.com/ ; trusted-types angular angular#bundler; require-trusted-types-for 'script'; ")
	w.Header().Set("Strict-Transport-Security", "max-age=63072000")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-Frame-Options", "DENY")
	w.Header().Set("X-XSS-Protection", "1; mode=block")
	w.Header().Set("Referrer-Policy", "strict-origin")
}
