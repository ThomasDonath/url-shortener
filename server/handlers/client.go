package handlers

import (
	"net/http"
	"url-shortener/pages"
)

// handle *all* requests to the client which is a Singe-Page-App
// as client-side-routing will be handled inside the client app we have only to response with the index.html
func Client(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	setHeaders(w)

	rawFile, _ := pages.AngularFiles.ReadFile("dist/index.html")
	w.Write(rawFile)
}
