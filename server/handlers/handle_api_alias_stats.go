package handlers

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/google/uuid"

	"url-shortener/def"
	"url-shortener/logger"
	"url-shortener/store"
	"url-shortener/userContext"
)

func HandleApiAliasStats(w http.ResponseWriter, r *http.Request) {
	logger.Log.Debug("called Stats api")

	// user, role from JWT
	userCtx, userFound := userContext.FromUserContext(r.Context())
	if !userFound {
		logger.Log.Error("server error: no user context found")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("server error: no user context found"))
		return
	}

	params, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		logger.Log.Error(fmt.Sprintf("server error getting parameters %+v", err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("server error getting parameters %+v", err)))
		return
	}

	paramAliasId := params.Get("AliasId")
	aliasId, idErr := uuid.Parse(paramAliasId)
	if idErr != nil {
		logger.Log.Error(fmt.Sprintf("server error getting parameters %+v", idErr))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("server error getting parameters %+v", idErr)))
		return
	}
	paramFormat := params.Get("format")
	paramSortColumn := params.Get("sortBy")
	paramSortOrder := params.Get("sortOrder")
	paramPageNumber := params.Get("pageNumber")
	pageNumber, _ := strconv.ParseUint(paramPageNumber, 10, 0)
	paramPageSize := params.Get("pageSize")
	pageSize, _ := strconv.ParseUint(paramPageSize, 10, 0)

	logger.Log.Debug(fmt.Sprintf("call to store.GetStatistics() with Params %s, %s, %s, %s, %d, %d", paramAliasId, paramFormat, paramSortColumn, paramSortOrder, pageNumber, pageSize))

	var retv def.ApiStatisticsReturnType
	retv.Data, retv.RowCount, err = store.GetStatistics(
		userCtx.GrantedRole,
		aliasId,
		paramFormat,
		paramSortColumn,
		paramSortOrder,
		pageNumber,
		pageSize)
	if err != nil {
		if err.Error() == "user is not allowed to query statistics" {
			w.WriteHeader(http.StatusForbidden)
		} else {
			logger.Log.Error(fmt.Sprintf("server error %+v", err))
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("server error %+v", err)))
		}
	} else {
		w.Header().Add("Content-Type", "application/json; charset=utf-8")
		struct2response(http.StatusOK, retv, w)
	}
}
