package def

import "github.com/google/uuid"

type ApiStatisticsDetailsType struct {
	Continent string `json:"continent"`
	Country   string `json:"country"`
	City      string `json:"city"`
	Agent     string `json:"agent"`
	Role      string `json:"role"`
	Result    string `json:"result"`
	Count     uint64 `json:"count"`
}

type ApiStatisticsType struct {
	Id        uuid.UUID                  `json:"id"`
	Date_time string                     `json:"dateTime"`
	Count     uint64                     `json:"count"`
	Details   []ApiStatisticsDetailsType `json:"details"`
}

type ApiStatisticsReturnType struct {
	Data     []ApiStatisticsType `json:"data"`
	RowCount uint                `json:"rowCount"`
}
