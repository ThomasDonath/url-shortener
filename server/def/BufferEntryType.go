package def

import "github.com/google/uuid"

// in-memory buffer to response to already know aliases as fast as possible
type BufferEntryType struct {
	FullUrl string
	Public  bool
	Id      uuid.UUID
}
