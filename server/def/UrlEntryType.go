package def

import (
	"github.com/google/uuid"
)

// structure in the database and client app.
//
// ExpiryDate is a string conform to Date.toJSON()
type UrlEntryType struct {
	Id          uuid.UUID `json:"id"`
	FullUrl     string    `json:"fullUrl"`
	Alias       string    `json:"alias"`
	Public      bool      `json:"public"`
	Owner       string    `json:"owner"`
	ExpiryDate  string    `json:"expiryDate"`
	Description string    `json:"description"`
}

func NewUrlEntryType(fullUrl string, alias string, public bool, owner string, expiryDate string, description string) UrlEntryType {
	return UrlEntryType{
		Id:          uuid.Nil,
		Alias:       alias,
		FullUrl:     fullUrl,
		Public:      public,
		Owner:       owner,
		ExpiryDate:  expiryDate,
		Description: description,
	}
}

func NewUrlEntryEmptyType() UrlEntryType {

	return UrlEntryType{
		Id:          uuid.Nil,
		Alias:       "",
		FullUrl:     "",
		Public:      false,
		Owner:       "",
		ExpiryDate:  "",
		Description: "",
	}
}
