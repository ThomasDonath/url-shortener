package def

// will be implemented in client/src/app/model/AliasApiReturnType.interface.ts too - hold in synch!!!

// Errorcodes to be used in ApiReturnType
const ErrorCodeOk uint = 0
const ErrorCodeForbidden uint = 101
const ErrorCodeInsert uint = 200
const ErrorCodeAliasInUse uint = 201

// structered answer for my API calls to handle app errors in the client while technical errors (expressed by HTTP status code) will be handle seperated
type ApiReturnType struct {
	StatusCode  uint         `json:"statusCode"`
	MessageText string       `json:"messageText"`
	Data        UrlEntryType `json:"data"`
	RowCount    uint         `json:"rowCount"`
}

func NewApiReturnType(c uint, m string, d UrlEntryType) ApiReturnType {
	return ApiReturnType{
		StatusCode:  c,
		MessageText: m,
		Data:        d,
	}
}
