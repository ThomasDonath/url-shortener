package def

// hold this in synch with client/src/app/model/roles.ts!!!
// see README.adoc in project for security concept
var ROLES = map[string]int{
	"God":           1000, // for internal actions only
	"Administrator": 9,
	"Editor":        6,
	"Viewer":        3,
	"Public":        0,
}
