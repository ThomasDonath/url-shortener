package def

const UrlApiAlias = "/api/alias/"
const UrlApiAliasStats = "/api/alias-stats/"
const UrlHealthCheck = "/healthz/"
const UrlClientApp = "/client/"
const UrlClientFiles = "/clientfiles/"
const UrlClientConfig = "/clientconfig/"
