package def

type ApiAliasesReturnType struct {
	Data     []UrlEntryType `json:"data"`
	RowCount uint           `json:"rowCount"`
}
