.SILENT:

ifeq ($(strip $(MYSQL_ROOT_PASSWORD)),)
$(error "ERROR set MYSQL_ROOT_PASSWORD first (run set.private)")
endif
ifeq ($(strip $(SHORTY_DB_PWD)),)
$(error "ERROR set SHORTY_DB_PWD first (run set.private)")
endif

ifeq ($(strip $(SHORTY_AAD_TENANT)),)
$(error "ERROR set SHORTY_AAD_TENANT first (run set.private)")
endif
ifeq ($(strip $(SHORTY_AAD_CLIENT_ID)),)
$(error "ERROR set SHORTY_AAD_CLIENT_ID first (run set.private)")
endif

ifeq ($(strip $(SHORTY_GeoIp_ACCOUNT)),)
$(error "ERROR set SHORTY_GeoIp_ACCOUNT first (run set.private)")
endif
ifeq ($(strip $(SHORTY_GeoIp_TOKEN)),)
$(error "ERROR set SHORTY_GeoIp_TOKEN first (run set.private)")
endif

K8S_NAMESPACE=td-shorty
K8S_POD_INSTANCE=dev
K8S_POD_NAME=url-shortener
K8S_POD_QUERY = kubectl get pod -n $(K8S_NAMESPACE) --selector="app.kubernetes.io/instance=$(K8S_POD_INSTANCE),app.kubernetes.io/name=$(K8S_POD_NAME)" --field-selector=status.phase=Running --output jsonpath='{.items[0].metadata.name}'
K8S_TAG = latest

# Configure Helm Deployment
HELM_IMAGE_REPO=eu.gcr.io/gematik-all-infra-prod/spielwiese/td-shorty
HELM_IMAGE_TAG=latest
HELM_PROXY_REPO=eu.gcr.io/cloud-sql-connectors/cloud-sql-proxy
HELM_PROXY_TAG=2

build: #build-client
	# as we use "latest" we have to pull explictly
	docker pull node
	docker pull golang

	docker build -t url-shortener:"$$(cat VERSION)-local" -f deployment/docker/Dockerfile \
			--build-arg APP_VERSION="$$(cat VERSION)-local" \
			--build-arg CURRENT_TIMESTAMP=$$(date -Iseconds) \
			.

aad:
	cd deployment/azuread.tf
	gcloud container clusters get-credentials shared-k8s-dev --zone europe-west3-a  --project gematik-all-k8s-db-dev
	./install-DEV.sh
	
.ONESHELL:	
k8s:
	gcloud container clusters get-credentials shared-k8s-dev --zone europe-west3-a  --project gematik-all-k8s-db-dev

	# upload image, get proxy for inspect
	docker pull registry.gitlab.com/thomasdonath/url-shortener:$(K8S_TAG)
	docker tag registry.gitlab.com/thomasdonath/url-shortener:$(K8S_TAG) $(HELM_IMAGE_REPO):$(HELM_IMAGE_TAG)
	docker push $(HELM_IMAGE_REPO):$(HELM_IMAGE_TAG)
	docker pull $(HELM_PROXY_REPO):$(HELM_PROXY_TAG)

	# get digests
	HELM_IMAGE_DIGEST=$$(docker inspect --format='{{index .RepoDigests 0}}' $(HELM_IMAGE_REPO):$(HELM_IMAGE_TAG) | sed -E 's/(^.*)@(.*)/\2/')
	HELM_PROXY_DIGEST=$$(docker inspect --format='{{index .RepoDigests 0}}' $(HELM_PROXY_REPO):$(HELM_PROXY_TAG) | sed -E 's/(^.*)@(.*)/\2/')
	echo using Main Image $(HELM_IMAGE_REPO):$(HELM_IMAGE_TAG)@$${HELM_IMAGE_DIGEST}
	echo using SQLProxy Image $(HELM_PROXY_REPO):$(HELM_PROXY_TAG)@$${HELM_PROXY_DIGEST}

	# generate secrets hashes
	SECRET_AAD_VERSION=$$(kubectl get secret aad-config -n $(K8S_NAMESPACE) -o custom-columns=RSRC:.metadata.resourceVersion --no-headers=true)
	SECRET_DB_VERSION=$$(kubectl get secret mysql-shorty -n $(K8S_NAMESPACE) -o custom-columns=RSRC:.metadata.resourceVersion --no-headers=true)

	helm upgrade --install dev ./deployment/k8s.helm \
		--namespace $(K8S_NAMESPACE) \
		--set image.repository=$(HELM_IMAGE_REPO) \
		--set image.tag=$(HELM_IMAGE_TAG) \
		--set image.digest=$${HELM_IMAGE_DIGEST} \
		--set gcpSqlProxy.imageName=$(HELM_PROXY_REPO) \
		--set gcpSqlProxy.imageTag=$(HELM_PROXY_TAG) \
		--set gcpSqlProxy.digest=$${HELM_PROXY_DIGEST} \
		--set hostname="shorty" \
		--set stage="dev" \
		--set secretConfig.aad.version="$${SECRET_AAD_VERSION}" \
		--set secretConfig.mysql.version="$${SECRET_DB_VERSION}" \
		--set BrowserTitle="DevShorty" \
		--set domain="ccs.gematik.solutions" \
		--set GeoIp_ACCOUNT=${SHORTY_GeoIp_ACCOUNT} \
		--set GeoIp_TOKEN=${SHORTY_GeoIp_TOKEN} \
		--set ipName="shorty-ip" \
		--set mysql_port_enabled=true \
		--set log_level=INFO \
		--set withTestData=false 

k8s-log:
	kubectl logs -f $$($(K8S_POD_QUERY)) -n $(K8S_NAMESPACE)

k8s-local:
	kubectl port-forward $$($(K8S_POD_QUERY)) -n $(K8S_NAMESPACE) 8080:8080

k8s-mysql:
	kubectl port-forward $$($(K8S_POD_QUERY)) -n $(K8S_NAMESPACE) 3306:3306

.ONESHELL:
run:
	export SHORTY_IMAGE="url-shortener"

	export MYSQL_ROOT_PASSWORD=$(MYSQL_ROOT_PASSWORD)
	export SHORTY_DB_PWD=$(SHORTY_DB_PWD)
	export SHORTY_LOG_LEVEL="INFO"
	export SHORTY_DEBUG_DATA="true"

	export SHORTY_GeoIp_API=true
	export SHORTY_GeoLite=true
	export SHORTY_DEBUG_FULL_DATA="false"

	export SHORTY_AAD_SCOPE="api://shorty.dev.thomasdonathgematik.onmicrosoft.com/access"

	docker-compose -f deployment/docker/docker-compose.yaml up
	docker-compose -f deployment/docker/docker-compose.yaml down -v

.ONESHELL:
run-gl:
	export SHORTY_IMAGE="registry.gitlab.com/thomasdonath/url-shortener"
	docker pull $${SHORTY_IMAGE}

	export MYSQL_ROOT_PASSWORD=$(MYSQL_ROOT_PASSWORD)
	export SHORTY_DB_PWD=$(SHORTY_DB_PWD)
	export SHORTY_LOG_LEVEL="INFO"
	export SHORTY_DEBUG_DATA="true"
	export SHORTY_GeoIp_API=true
	export SHORTY_GeoLite=true
	export SHORTY_DEBUG_FULL_DATA="false"
	export SHORTY_AAD_SCOPE="api://shorty.dev.thomasdonathgematik.onmicrosoft.com/access"

	docker-compose -f deployment/docker/docker-compose.yaml up
	docker-compose -f deployment/docker/docker-compose.yaml down

build-client:
	cd client
	ng build
	cp dist/url-shortener/* ../server/pages/dist/.

test-performance:
	rm -rf test.jmeter/results
	mkdir test.jmeter/results
	jmeter -n -t test.jmeter/shorty.jmx -l test.jmeter/results/results.log -e -o test.jmeter/results

# to use use fonts from local instead of online
# fonts:
# 	# download styleshett and fonts for Google fonts
# 	curl -o client/src/styles/roboto.css "https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap"
# 	curl -o client/src/styles/material-icons.css "https://fonts.googleapis.com/icon?family=Material+Icons"

# 	# MacOS - not! Linux
# 	cat client/src/styles/*.css | grep -o -e 'https://.*.ttf' 
# 	echo download these files and replace their path in styles/*.css

