variable "stage" {}
variable "OWNER" {}
variable "ARM_TENANT_ID" {}
variable "tenant_domain" {}

variable "host_fqdn" {}
variable "redirect_host_fqdn" { type = list(string) }

variable "GITLAB_ADDRESS" {}
variable "GITLAB_USERNAME" {}
variable "GITLAB_PASSWORD" {}

variable "k8s_namespace" {}

locals {
	identifier_uri = "api://shorty.${var.stage}.${var.tenant_domain}"
}
