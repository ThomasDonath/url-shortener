resource "random_uuid" "app_role_admin" {}
resource "random_uuid" "app_role_viewer" {}
resource "random_uuid" "app_role_editor" {}
resource "random_uuid" "oauth_scope" {}

resource "azuread_application" "app" {
  display_name            = "thomas.url-shortener" 
  owners                  = [data.azuread_user.tdo.object_id, data.azuread_client_config.current.object_id]
  sign_in_audience        = "AzureADMyOrg"
  prevent_duplicate_names = true
  group_membership_claims = ["None"]

  identifier_uris = [local.identifier_uri]

  web {
    homepage_url = "https://${var.host_fqdn}"
    implicit_grant {
      access_token_issuance_enabled = false
      id_token_issuance_enabled     = true
    }
  }
  single_page_application {
    redirect_uris = var.redirect_host_fqdn
  }

  app_role {
    id                   = random_uuid.app_role_admin.result
    display_name         = "Administrators"
    description          = "Administrators"
    enabled              = true
    allowed_member_types = ["User"]
    value                = "Administrator"
  }
  app_role {
    id                   = random_uuid.app_role_viewer.result
    display_name         = "Viewers"
    description          = "Viewers (Read Only Users)"
    enabled              = true
    allowed_member_types = ["User"]
    value                = "Viewer"
  }
  app_role {
    display_name         = "Editors"
    description          = "Editors"
    enabled              = true
    allowed_member_types = ["User"]
    id                   = random_uuid.app_role_editor.result
    value                = "Editor"
  }

  required_resource_access {
    resource_app_id = data.azuread_application_published_app_ids.well_known.result.MicrosoftGraph

    resource_access {
      id   = azuread_service_principal.msgraph.oauth2_permission_scope_ids["email"]
      type = "Scope"
    }
    resource_access {
      id   = azuread_service_principal.msgraph.oauth2_permission_scope_ids["User.Read"]
      type = "Scope"
    }
    resource_access {
      id   = azuread_service_principal.msgraph.oauth2_permission_scope_ids["profile"]
      type = "Scope"
    }
  }

  optional_claims {
    id_token {
      additional_properties = [
        "include_externally_authenticated_upn",
      ]
      essential = false
      name      = "upn"
    }
  }

  api {
    known_client_applications      = []
    mapped_claims_enabled          = false
    requested_access_token_version = 2

    oauth2_permission_scope {
      admin_consent_description  = "access"
      admin_consent_display_name = "access"
      enabled                    = true
      id                         = random_uuid.oauth_scope.result
      type                       = "Admin"
      value                      = "access"
    }
  }
}
data "azuread_application_published_app_ids" "well_known" {}
resource "azuread_service_principal" "msgraph" {
  client_id = data.azuread_application_published_app_ids.well_known.result.MicrosoftGraph
  use_existing   = true
}
