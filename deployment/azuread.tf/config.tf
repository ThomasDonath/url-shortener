data "terraform_remote_state" "state" {
  backend = "http"

  config = {
    address  = var.GITLAB_ADDRESS
    username = var.GITLAB_USERNAME
    password = var.GITLAB_PASSWORD
  }
}

terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.47.0"
    }
  }
  
  backend "http" {}
}
