resource "azuread_service_principal" "sp" {
  client_id = azuread_application.app.client_id
  owners         = [data.azuread_user.tdo.object_id, data.azuread_client_config.current.object_id]
  notes          = "Shorty"

  account_enabled              = true # must be enabled to get a token - even for demon apps
  app_role_assignment_required = true

  tags = [
    "gematik.de",
    "WindowsAzureActiveDirectoryIntegratedApp",
    "apiConsumer",
    "singlePageApp",
    "webApi",
    "webApp",
  ]

}
