resource "kubernetes_secret" "aad-config" {
  metadata {
    name      = "aad-config"
    namespace = var.k8s_namespace
  }

  data = {
    tenant_id      = var.ARM_TENANT_ID
    application_id = azuread_application.app.client_id
    scope          = "${local.identifier_uri}/access"
  }
}
