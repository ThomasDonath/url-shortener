# set environment and reconfigure Terraform backend for stage DEV

export TF_VAR_stage=dev

#########################################################
# configure Terraform and Terraform state using variables
. ./set-env-${TF_VAR_stage}.private
# export ARM_CLIENT_ID="***"  # service accout 
# export ARM_TENANT_ID="***"
# export GITLAB_PROJECT_ID="***"
# export TF_VAR_GITLAB_USERNAME="***"
# export TF_VAR_GITLAB_PASSWORD="***"

##############################################
# configure Terraform variables for this stage
export TF_VAR_OWNER="thomas.donath_gematik.de#EXT#@thomasdonathgematik.onmicrosoft.com"
export TF_VAR_tenant_domain="thomasdonathgematik.onmicrosoft.com"

export TF_VAR_k8s_namespace=td-shorty
export TF_VAR_host_fqdn="localhost"
export TF_VAR_redirect_host_fqdn="[\"http://localhost:4200/client/logged-in/\",\"http://localhost:8080/client/logged-in/\",\"https://shorty.${TF_VAR_stage}.ccs.gematik.solutions/client/logged-in/\"]"

# initialize Terraform (terraform init)
. ./set-env-include.sh
