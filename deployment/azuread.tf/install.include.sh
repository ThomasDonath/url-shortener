# run deployment with pre-set environment
# to be called from ./set-env-STAGE.sh

. ./set-env-${TF_VAR_stage}.sh

#terraform init will be done inside of set-env-*.sh
terraform apply
