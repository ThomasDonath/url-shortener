# url-shortener

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.2.

prepared with

```shell
npm install -g @angular/cli
ng new my-app
ng add @angular/material
```

based on https://blog.angular-university.io/angular-material-data-table/
started with https://therichpost.com/angular-13-angular-material-datatable-integration/

https://blog.angular-university.io/how-to-build-angular2-apps-using-rxjs-observable-data-services-pitfalls-to-avoid/


AzureAD see

* https://angular.de/artikel/oauth-odic-plugin/
* https://github.com/manfredsteyer/angular-oauth2-oidc
* https://github.com/jeroenheijmans/sample-angular-oauth2-oidc-with-auth-guards
