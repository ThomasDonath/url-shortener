import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';

import { IStateService } from '../../services/state-service.service.interface';
import { IOAuthService } from '../../services/oauth.service.interface';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    standalone: false
})
export class LoginComponent {

  protected DSP_ORIGIN: string;
  protected alias2go2: string = '';

  constructor(
    private fb: FormBuilder,
    private router: Router,
    protected state: IStateService,
    private authService: IOAuthService) {
    this.DSP_ORIGIN = window.location.origin;
  }

  submit() {
    if (this.alias2go2) {
      location.href = `${window.location.origin}/${this.alias2go2}`;
    } else {
      alert('Please enter an alias before')
    }
  }

  login() {
    this.authService.initLoginFlow();
  }
  logout() {
    this.state.logout();
  }
  gotoAliasesPage() {
    this.router.navigateByUrl('alias')
  }
  protected isLoggedIn(): boolean {
    return this.state.isLoggedIn()
  }
}
