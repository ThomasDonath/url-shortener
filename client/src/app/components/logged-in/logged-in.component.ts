import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { IOAuthService } from '../../services/oauth.service.interface';
import { IStateService } from '../../services/state-service.service.interface';

@Component({
    selector: 'app-logged-in',
    templateUrl: './logged-in.component.html',
    styleUrls: ['./logged-in.component.css'],
    standalone: false
})
export class LoggedInComponent implements OnInit, AfterViewInit {

  debug: boolean = false;
  userName: string = '';
  idToken: string = '';

  constructor(protected authService: IOAuthService, private stateService: IStateService, private router: Router, private cookieService: CookieService) { }

  ngOnInit(): void {
    const claimI = this.authService.getIdentityClaims();
    const claimS = this.authService.getIdToken();

    if (!claimI) {
      this.userName = "empty"
    } else {
      this.userName = JSON.stringify(claimI['preferred_username']);
    }
    if (!claimS) {
      this.idToken = "empty"
    } else {
      this.idToken = JSON.stringify(claimI['roles']);
    }
  }

  async ngAfterViewInit() {
    // after login we should wait a little for processing
    let i: number = 0;
    while (!this.authService.hasValidIdToken() && i < 20) {
      await new Promise(resolve => setTimeout(resolve, 500));
      i++;
    }

    this.stateService.UserName = this.authService.getIdentityClaims()['preferred_username'];
    this.stateService.GrantedRole = this.authService.getIdentityClaims()['roles'][0]; //FIXME if there is more than entry, return the max one
    this.cookieService.set('auth', this.authService.getAccessToken(), {expires: 1, sameSite: 'Strict', path: '/', secure: true}) //FIXME: should be updated with every refresh!

    if (this.debug) console.debug('route to List')
    else this.router.navigateByUrl('alias');
  }

  logout() {
    this.stateService.logout();
  }
}