import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IOAuthService } from '../../services/oauth.service.interface';
import { OAuthMockService } from '../../services/oauth.service.mock';
import { IStateService } from '../../services/state-service.service.interface';
import { StateMockService } from '../../services/state-service.service.mock';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { LoggedInComponent } from './logged-in.component';

describe('LoggedInComponent', () => {
  let component: LoggedInComponent;
  let fixture: ComponentFixture<LoggedInComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatProgressSpinnerModule,
      ],
      providers: [
        { provide: IOAuthService, useClass: OAuthMockService },
        { provide: IStateService, useClass: StateMockService },
      ],
      declarations: [LoggedInComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(LoggedInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });
});

