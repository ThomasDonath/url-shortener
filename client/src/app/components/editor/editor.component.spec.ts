import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { OAuthMockService } from '../../services/oauth.service.mock';
import { IOAuthService } from '../../services/oauth.service.interface';
import { IStateService } from '../../services/state-service.service.interface';
import { StateMockService } from '../../services/state-service.service.mock';
import { EditorParameters } from './editor.component'
import { IdNil } from '../../model/AliasesType.interface'

import { EditorComponent } from './editor.component';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';

describe('EditorComponent (Named Alias)', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;

  let params: EditorParameters = {
    isNamedAlias: true,
    alias: {
      id: IdNil,
      fullUrl: "",
      alias: "",
      public: false,
      owner: "",
      expiryDate: null,
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        BrowserAnimationsModule,
        MatIconModule,
        MatInputModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: IStateService, useClass: StateMockService },
        { provide: IOAuthService, useClass: OAuthMockService },
        { provide: MAT_DIALOG_DATA, useValue: params },
        { provide: MatDialogRef, useValue: {} }
      ],
      declarations: [EditorComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });
});