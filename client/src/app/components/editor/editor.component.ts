import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';

import { IAliasType } from '../../model/AliasesType.interface';

export interface EditorParameters {
  isNamedAlias: boolean,
  alias: IAliasType,
}

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.css'],
    standalone: false
})
export class EditorComponent {
  title: string = 'new';
  AliasEditable: boolean = false;
  protected showAliasItem: boolean;
  private alias: IAliasType;
  protected form: FormGroup;

  userIsAdmin: boolean = false;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private params: EditorParameters,
    private dialogRef: MatDialogRef<EditorComponent>) {

    this.alias = params.alias;

    if (params.isNamedAlias) {
      this.form = this.fb.group({
        id: [this.alias.id],
        fullUrl: [this.alias.fullUrl, Validators.required],
        alias: [this.alias.alias, Validators.required],
        public: [this.alias.public],
        expiryDate: [this.alias.expiryDate],
        owner: [this.alias.owner, Validators.required],
        description: [this.alias.description],
      });
      this.showAliasItem = true;
    } else {
      this.form = this.fb.group({
        id: [this.alias.id],
        fullUrl: [this.alias.fullUrl, Validators.required],
        public: [this.alias.public],
        expiryDate: [this.alias.expiryDate],
        owner: [this.alias.owner, Validators.required],
        description: [this.alias.description],
      });
      this.showAliasItem = false;
    }

    if (this.alias.alias) {
      this.title = this.alias.alias + ' = ' + (this.alias.fullUrl.substring(0, 30));
      this.AliasEditable = false;
    }
    else {
      this.title = 'new';
      this.AliasEditable = true;
    }
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }
}

export function openEditAliasDialog(dialog: MatDialog, alias: IAliasType, isNamedAlias: boolean, isAdmin: boolean) {

  const config = new MatDialogConfig();

  config.disableClose = true;
  config.autoFocus = true;
  config.panelClass = "modal-panel";
  config.backdropClass = "backdrop-modal-panel";

  const params: EditorParameters = { isNamedAlias: isNamedAlias, alias: alias }

  config.data = {
    ...params
  };

  const dialogRef = dialog.open(EditorComponent, config);
  dialogRef.componentInstance.userIsAdmin = isAdmin;

  return dialogRef.afterClosed();
}
