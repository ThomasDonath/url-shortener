import { AfterViewInit, Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { tap, filter, Observable, merge, fromEvent, debounceTime, distinctUntilChanged } from 'rxjs';

import { IAliasType, IdNil } from '../../model/AliasesType.interface'
import { AliasesDataSource } from './list.datasource';
import { MatDialog } from '@angular/material/dialog';
import { openEditAliasDialog } from '../editor/editor.component';

import { ROLES } from '../../model/roles'
import { IAliasesService } from '../../services/aliases-service.service.interface';
import { IOAuthService } from '../../services/oauth.service.interface';
import { Router } from '@angular/router';
import { openStatisticsDialog } from '../statistics/statistics.component';

@Component({
    selector: 'app-aliases-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    standalone: false
})
export class ListComponent implements OnInit, AfterViewInit {
  tableDataSource: AliasesDataSource;
  displayedColumns: string[] = ['select', 'alias', 'public', 'owner', 'fullUrl'];

  userName: string;
  grantedRole: number;

  selectedAlias: IAliasType;

  ROLES = ROLES;
  numberOfRows$: Observable<number>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filterAlias') filterAlias: ElementRef;
  @ViewChild('filterOwner') filterOwner: ElementRef;
  @ViewChild('filterUrl') filterUrl: ElementRef;

  constructor(
    private aliasesService: IAliasesService, 
    private authService: IOAuthService, 
    private EditorDialog: MatDialog,
    private statisticsDialog: MatDialog,
    private router: Router) {

    this.userName = this.authService.getIdentityClaims()['preferred_username'];
    this.grantedRole = this.authService.getIdentityClaims()['roles'][0]; //FIXME if there is more than entry, return the max one
  }

  ngOnInit() {
    this.tableDataSource = new AliasesDataSource(this.aliasesService);
    this.tableDataSource.loadAliases('', '', '', 'fullUrl', 'asc', 0, 15);
    this.numberOfRows$ = this.tableDataSource.rowCount$;
  }

  ngAfterViewInit() {
      // server-side search
      merge(
        fromEvent(this.filterUrl.nativeElement, 'keyup'),
        fromEvent(this.filterAlias.nativeElement, 'keyup'),
        fromEvent(this.filterOwner.nativeElement, 'keyup')
      )
        .pipe(
          debounceTime(150),
          distinctUntilChanged(),
          tap(() => {
            this.paginator.pageIndex = 0;
            this.loadAliasesPage();
          })
        )
        .subscribe();

      // reset the paginator after sorting
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.loadAliasesPage())
        )
        .subscribe();
  }

  loadAliasesPage() {
    this.numberOfRows$ = this.tableDataSource.rowCount$;
    this.tableDataSource.loadAliases(
      this.filterUrl.nativeElement.value,
      this.filterAlias.nativeElement.value,
      this.filterOwner.nativeElement.value,
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  goHome() {
    this.router.navigateByUrl('login')
  }

  protected roleHigherThanEditor(r: number): boolean {
    return this.ROLES[r] > this.ROLES["Editor"]
  }

  protected userIsAllowed2Edit(): boolean {
    return (
      (ROLES[this.grantedRole] == ROLES['Editor'] && this.selectedAlias.owner == this.userName) || this.ROLES[this.grantedRole] == this.ROLES["Administrator"]
    )
  }

  addRandomAlias() {
    let alias: IAliasType = {
      id: IdNil,
      fullUrl: '',
      alias: '',
      public: false,
      expiryDate: null,
      owner: this.userName,
      description: ''
    }

    if (this.ROLES[this.grantedRole] < this.ROLES["Editor"]) {
      alert('You are not allowed to add entries');
    } else {
      openEditAliasDialog(this.EditorDialog, alias, false, this.roleHigherThanEditor(this.grantedRole))
        .pipe(filter(val => !!val))
        .subscribe(val => {
          val.alias = '';
          this.aliasesService.createAlias$(val)
            .subscribe(val => {
              if (val.statusCode == 0) alert(`Your alias is ${val.data.alias}`)
              else alert(`error ${val.messageText}`)
              this.loadAliasesPage()
            }
            )
        })
    }
  }

  addNamedAlias() {
    let alias: IAliasType = {
      id: IdNil,
      fullUrl: '',
      alias: '',
      public: false,
      expiryDate: null,
      owner: this.userName,
      description: ''
    }

    if (this.ROLES[this.grantedRole] < this.ROLES["Editor"]) {
      alert('You are not allowed to add entries');
    } else {
      openEditAliasDialog(this.EditorDialog, alias, true, this.roleHigherThanEditor(this.grantedRole))
        .pipe(filter(val => !!val))
        .subscribe(val => {
          this.aliasesService.createAlias$(val)
            .subscribe(val => {
              if (val.statusCode != 0) alert(`Error ${val.messageText}`)
              this.loadAliasesPage()
            }
            )
        })
    }
  }

  editData(alias: IAliasType) {
    if (!(alias.owner === this.userName || this.roleHigherThanEditor(this.grantedRole))) {
      alert('You are not allowed to edit foreign entries');
    } else {
      openEditAliasDialog(this.EditorDialog, alias, true, this.roleHigherThanEditor(this.grantedRole))
        .pipe(
          filter(val => !!val)
        )
        .subscribe(val => {
          this.selectedAlias = val
          this.aliasesService.updateAlias$(val)
            .subscribe(val => this.loadAliasesPage()
            )
        })
    }
  }

  deleteData(alias: IAliasType) {
    if (!(alias.owner === this.userName || this.roleHigherThanEditor(this.grantedRole))) {
      alert('You are not allowed to edit foreign entries');
    } else {
      this.aliasesService.deleteAlias$(alias)
        .subscribe(val => this.loadAliasesPage()
        )
    }
  }

  showStats(alias: IAliasType) {
    if (!(alias.owner === this.userName || this.roleHigherThanEditor(this.grantedRole))) {
      alert('You are not allowed to work on foreign entries');
    } else {
      openStatisticsDialog(this.statisticsDialog, alias);
    }
  }
}
