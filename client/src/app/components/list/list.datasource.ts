import { DataSource, CollectionViewer } from "@angular/cdk/collections";
import { BehaviorSubject, Observable, catchError, of, finalize } from "rxjs";
import { IAliasType } from "src/app/model/AliasesType.interface";
import { IAliasesService } from "../../services/aliases-service.service.interface";

export class AliasesDataSource implements DataSource<IAliasType> {

    private aliasesSubject = new BehaviorSubject<IAliasType[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private rowCountSubject = new BehaviorSubject<number>(0);

    public loading$ = this.loadingSubject.asObservable();
    public rowCount$ = this.rowCountSubject.asObservable();

    errorMsg: string;

    constructor(private aliasesService: IAliasesService) {}

    connect(collectionViewer: CollectionViewer): Observable<IAliasType[]> {
        return this.aliasesSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.aliasesSubject.complete();
        this.loadingSubject.complete();
    }

    loadAliases(
        filterUrl: string, 
        filterAlias: string, 
        filterOwner: string, 
        sortColumn: string, 
        sortDirection: string, 
        pageIndex: number, 
        pageSize: number) {

        this.loadingSubject.next(true);

        this.aliasesService.getAll$(filterUrl, filterAlias, filterOwner, sortColumn, sortDirection, pageIndex, pageSize)
            .pipe(
                catchError(error => {
                    alert(error.message);
                    return of([]);
                }
                ),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(a => {
                this.aliasesSubject.next(a["data"])
                this.rowCountSubject.next(a["rowCount"]);
            });
    }
}