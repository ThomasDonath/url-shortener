import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { HarnessLoader } from '@angular/cdk/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { ListComponent } from './list.component';
import { IAliasesService } from '../../services/aliases-service.service.interface';
import { AliasesMockService } from '../../services/aliases-service.service.mock';
import { OAuthMockService } from '../../services/oauth.service.mock';
import { IOAuthService } from '../../services/oauth.service.interface';

describe('ListComponent', () => {
  let fixture: ComponentFixture<ListComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, MatDialogModule, NoopAnimationsModule, MatSortModule, MatPaginatorModule,
      ],
      providers: [
        { provide: IAliasesService, useClass: AliasesMockService },
        { provide: IOAuthService, useClass: OAuthMockService },
        HttpClient, HttpHandler],
      declarations: [
        ListComponent,
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(ListComponent);
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.documentRootLoader(fixture);
  });

  it('should create the component', () => {
    const fixture = TestBed.createComponent(ListComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
  /* FIXME: test button-disabled-logic
    it('all buttons disabled for "Viewer" role with alias selected', () => {
      const fixture = TestBed.createComponent(ListComponent);
      const { debugElement } = fixture;
      const { nativeElement } = debugElement;
  
  
      fixture.componentInstance.selectedAlias = {
        id: IdNil,
        fullUrl: 'testUrl',
        alias: 'a',
        public: false,
        owner: 'thomas',
      };
      fixture.componentInstance.grantedRole = ROLES.Administrator;
      fixture.detectChanges();
  
      const addRandomButton = debugElement.query(By.css('[data-testid="add-random-button"]'))
      expect(addRandomButton.nativeElement.disabled).toBe(true);
  
      const addButton = debugElement.query(By.css('[data-testid="add-button"]'))
      expect(addButton.nativeElement.disabled).toBe(true);
  
      const editButton = debugElement.query(By.css('[data-testid="edit-button"]'))
      expect(editButton.nativeElement.disabled).toBe(true);
  
      const deleteButton = debugElement.query(By.css('[data-testid="delete-button"]'))
      expect(deleteButton.nativeElement.disabled).toBe(true); 
    });
     */
});
