import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HarnessLoader } from '@angular/cdk/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from '@angular/material/table';

import { StatisticsComponent } from './statistics.component';
import { IStatisticsService } from '../../services/statistics.servise.interface';
import { StatisticsMockService } from '../../services/statistics.service.mock';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { HttpClient, HttpHandler, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { IOAuthService } from 'src/app/services/oauth.service.interface';
import { OAuthMockService } from '../../services/oauth.service.mock';

describe('StatisticsComponent', () => {
  let component: StatisticsComponent;
  let fixture: ComponentFixture<StatisticsComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [
        StatisticsComponent,
    ],
    imports: [MatDialogModule,
        NoopAnimationsModule,
        MatSortModule,
        MatPaginatorModule,
        MatSelectModule,
        MatTableModule],
    providers: [
        { provide: IStatisticsService, useClass: StatisticsMockService },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: IOAuthService, useClass: OAuthMockService },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
    ]
}).compileComponents();

    let httpTestingController: HttpTestingController;
    let httpClient: HttpClient;
    let httpHandler: HttpHandler;

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);

    fixture = TestBed.createComponent(StatisticsComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
    // loader = TestbedHarnessEnvironment.documentRootLoader(fixture);
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });
});
