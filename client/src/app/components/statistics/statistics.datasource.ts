import { DataSource, CollectionViewer } from "@angular/cdk/collections";
import { BehaviorSubject, Observable, map, catchError, of, finalize, tap } from "rxjs";

import { IStatisticsApiReturnType, IStatisticsType } from "../../model/StatisticsType.interface";
import { IStatisticsService } from "../../services/statistics.servise.interface";

export class StatisticsDataSource implements DataSource<IStatisticsType> {

    private dataSubject = new BehaviorSubject<IStatisticsType[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private rowCountSubject = new BehaviorSubject<number>(0);

    public loading$ = this.loadingSubject.asObservable();
    public rowCount$ = this.rowCountSubject.asObservable();

    errorMsg: string;

    constructor(private statisticsService: IStatisticsService) {}

    connect(collectionViewer: CollectionViewer): Observable<IStatisticsType[]> {
        return this.dataSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete();
        this.loadingSubject.complete();
    }

    loadStatistics(
        aliasId: string, 
        format: string, 
        sortBy: string,
        sortOrder: string,
        pageIndex: number, 
        pageSize: number) {

        this.loadingSubject.next(true);

        this.statisticsService.getAll$(aliasId, format, sortBy, sortOrder, pageIndex, pageSize)
            .pipe(
                catchError(error => {
                    alert(error.message);
                    return of([]);
                }
                ),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((a) =>{ 
                this.dataSubject.next(a["data"]);
                this.rowCountSubject.next(a["rowCount"]);
        });
    }
}
