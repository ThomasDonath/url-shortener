import { AfterViewInit, Component, Inject, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort, MatSortModule, MAT_SORT_DEFAULT_OPTIONS } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { merge, Observable, tap } from 'rxjs';
import { IAliasType } from '../../model/AliasesType.interface';
import { IStatisticsDetailsType, IStatisticsType } from '../../model/StatisticsType.interface';
import { IStatisticsService } from '../../services/statistics.servise.interface';
import { StatisticsDataSource } from './statistics.datasource';

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.css'],
    standalone: false
})
export class StatisticsComponent implements OnInit, AfterViewInit {

  protected tableDataSource: StatisticsDataSource;
  protected detailsDataSource: MatTableDataSource<IStatisticsDetailsType>

  protected title: string;
  protected selectedDateRow: IStatisticsType;
  protected displayedColumnsDate: string[] = ['select', 'dateTime', "count"];
  protected displayedColumnsDetails: string[] = ['continent', 'country', 'city', 'agent', 'role', 'result', 'count'];
  protected numberOfDateRows$: Observable<number>;

  protected format2query = 'day';

  @ViewChild('paginatorMaster') paginatorMaster: MatPaginator;
  @ViewChild('paginatorDetail') paginatordDetails: MatPaginator;
  @ViewChild('formatSelector') formatSelector: MatSelect;
  
  // THE PROBLEM: if I query with the diretive the Mat-Sort will be found but I have two sorters
  // works: @ViewChild(MatSort) sortDetails: MatSort
  // don't work: @ViewChild('sortDetails') sortDetails: MatSort
  // my workaround it to use @ViewChildren
  
  @ViewChildren(MatSort) sorters: QueryList<MatSort>;
  sortMaster: MatSort;
  sortDetails: MatSort;

  constructor(
    @Inject(MAT_DIALOG_DATA) private alias: IAliasType,
    private dialogRef: MatDialogRef<StatisticsComponent>,
    private statisticsService: IStatisticsService) {

    if (this.alias) {
      this.title = 'statistics for ' + alias.alias;
    }
  }

  ngOnInit(): void {
    this.tableDataSource = new StatisticsDataSource(this.statisticsService);
    this.tableDataSource.loadStatistics(this.alias.id, this.format2query, "1", "desc", 0, 10);
    this.numberOfDateRows$ = this.tableDataSource.rowCount$;

    this.detailsDataSource = new MatTableDataSource([])
  }

  ngAfterViewInit() {
    this.sortMaster = this.sorters.first;
    this.sortDetails = this.sorters.last;
    // reset the paginator after sorting
    this.sortMaster.sortChange.subscribe(() => this.paginatorMaster.pageIndex = 0);

    merge(this.sortDetails.sortChange, this.sortMaster.sortChange, this.paginatorMaster.page, this.formatSelector.valueChange)
      .pipe(
        tap(() => this.loadPage())
      )
      .subscribe();
  }

  switchDetails(r: IStatisticsType) {
    this.selectedDateRow = r;
    this.detailsDataSource.data = this.selectedDateRow.details;
    this.detailsDataSource.sort = this.sortDetails;
  }

  loadPage() {
    this.tableDataSource.loadStatistics(
      this.alias.id,
      this.format2query,
      this.sortMaster.active == 'count' ? '2' : '1',
      this.sortMaster.direction ? this.sortMaster.direction : 'desc',
      this.paginatorMaster.pageIndex,
      this.paginatorMaster.pageSize);
    this.detailsDataSource.paginator = this.paginatordDetails;
    this.detailsDataSource.sort = this.sortDetails;
  }

  close() {
    this.dialogRef.close();
  }
}

export function openStatisticsDialog(dialog: MatDialog, alias: IAliasType) {

  const config = new MatDialogConfig();

  config.disableClose = true;
  config.autoFocus = true;
  config.panelClass = "modal-panel";
  config.backdropClass = "backdrop-modal-panel";
  config.height = '80%',
    config.width = '100%',

    config.data = {
      ...alias
    };

  const dialogRef = dialog.open(StatisticsComponent, config);

  return dialogRef.afterClosed();
}