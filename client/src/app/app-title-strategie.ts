import { Injectable } from "@angular/core";
import { RouterStateSnapshot, TitleStrategy } from "@angular/router";

import { IConfigService } from "./services/config.service.interface";

@Injectable()
export class TemplatePageTitleStrategy extends TitleStrategy {

    constructor(private config: IConfigService) {
        super();
    }

    override updateTitle(routerState: RouterStateSnapshot) {
        const title = this.buildTitle(routerState);
        let browserTitleName: string;

        this.config.GotConfig.asObservable().subscribe(
            (v) => {
                if (v) {
                    browserTitleName = this.config.BrowserTitle.getValue();
                    if (title !== undefined) {
                        document.title = `${browserTitleName} - ${title}`;
                    } else {
                        document.title = `${browserTitleName} - Home`;
                    };
                }
            })
    };
}