import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListComponent } from './components/list/list.component';
import { EditorComponent } from './components/editor/editor.component';
import { IsLoggedInGuard } from './services/is-logged-in.guard';
import { LoginComponent } from './components/login/login.component';
import { LoggedInComponent } from './components/logged-in/logged-in.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/login', pathMatch: 'full'
  },
  {
    path: 'alias',
    component: ListComponent,
    canActivate: [IsLoggedInGuard],
    title: 'Aliases',
  },
  {
    path: 'login',
    component: LoginComponent,
    title: 'Home',
  },
  {
    path: 'logged-in',
    component: LoggedInComponent,
    title: 'Logged-in',
  },
  {
    path: "**",
    redirectTo: 'alias'  
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
