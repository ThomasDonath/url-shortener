import { Component } from '@angular/core';
import { filter } from 'rxjs';
import { authConfig } from 'src/environments/oauth-config';
import { IConfigService } from './services/config.service.interface';
import { IOAuthService } from './services/oauth.service.interface';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    standalone: false
})
export class AppComponent {
  title = 'Shorty';

  constructor(private oauthService: IOAuthService, private config: IConfigService) {
    this.config.GotConfig
      .pipe(filter((b) => b))
      .subscribe(
        (b) => {
          this.configureOAuth(this.config.AadTenantId.getValue(), this.config.AadClientId.getValue(), this.config.AadScope.getValue());

          oauthService.configure(authConfig);
          oauthService.loadDiscoveryDocumentAndTryLogin();
          oauthService.setupAutomaticSilentRefresh();

          this.title = config.BrowserTitle.getValue();
        })
  }

  private configureOAuth(issuer: string, clientId: string, scope: string): void {
    authConfig.issuer = 'https://login.microsoftonline.com/' + issuer + '/v2.0';
    authConfig.clientId = clientId;
    authConfig.scope = 'openid profile email offline_access ' + scope;
  }
}