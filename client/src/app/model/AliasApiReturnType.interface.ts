import { IAliasType } from "./AliasesType.interface"
// is a "copy" of server/def/ApiReturnType.go - hold in synch! (interface and constants)

export interface IAliasApiReturnType {
    statusCode?: number,
    messageText?: string,
    data?: IAliasType,
}

export const ErrorCodeOk: number = 0
export const ErrorCodeForbidden: number = 101
export const ErrorCodeInsert: number = 200
export const ErrorCodeAliasInUse: number = 201
