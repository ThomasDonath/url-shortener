// hold this in synch with server/def/role.go
export const ROLES = {
	"God":           1000, // for internal actions only
	"Administrator": 9,
	"Editor":        6,
	"Viewer":        3,
	"Public":        0,
}
