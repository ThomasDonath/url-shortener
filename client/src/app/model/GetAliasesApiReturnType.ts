import { IAliasType } from "./AliasesType.interface";

export interface IGetAliasesApiReturnType {
    data: IAliasType[],
    rowCount: number,
}