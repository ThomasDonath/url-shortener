export interface IStatisticsDetailsType {
    continent: string,
    country: string,
    city: string,
    agent: string,
    role: string,
    result: string,
    count: bigint
}
export interface IStatisticsType {
    dateTime: string,
    count: bigint,
    details: IStatisticsDetailsType[]
}
export interface IStatisticsApiReturnType {
    data: IStatisticsType[],
    rowCount: number,
}