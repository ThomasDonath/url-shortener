export interface IAliasType {
  id: string,
  fullUrl: string;
  alias?: string;
  public: boolean;
  owner: string;
  expiryDate: Date;
  description?: string;
}
export const IdNil: string = '00000000-0000-0000-0000-000000000000'