import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IStatisticsApiReturnType, IStatisticsType } from '../model/StatisticsType.interface';
import { IOAuthService } from './oauth.service.interface';
import { IStatisticsService } from './statistics.servise.interface';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService implements IStatisticsService {
  private ApiUrl: string = '';

  headers: HttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + this.authService.getAccessToken());

  constructor(private http: HttpClient, private authService: IOAuthService) {
    this.ApiUrl = window.location.origin + '/api/alias-stats/';
  }

  getAll$(
    aliasId: string,
    format: string,
    sortBy: string,
    sortOrder: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<IStatisticsApiReturnType> {

    let reqOptions = {
      headers: this.headers,
      params: new HttpParams()
        .set('AliasId', aliasId)
        .set('format', format)
        .set("sortBy", sortBy)
        .set("sortOrder", sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
    }

    return this.http.get<IStatisticsApiReturnType>(this.ApiUrl, reqOptions);
  }
}
