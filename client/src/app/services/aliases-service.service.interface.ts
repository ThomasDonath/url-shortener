import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/internal/Observable';

import { IAliasType } from 'src/app/model/AliasesType.interface';
import { IAliasApiReturnType } from '../model/AliasApiReturnType.interface';
import { IGetAliasesApiReturnType } from '../model/GetAliasesApiReturnType';

@Injectable()
export abstract class IAliasesService {

  abstract getAll$(
    filterUrl: string,
    filterAlias: string,
    filterOwner: string,
    sortColumn: string,
    sortOrder: string,
    pageNumber: number,
    pageSize: number
  ): Observable<IGetAliasesApiReturnType[]>;

  abstract createAlias$(a: IAliasType): Observable<IAliasApiReturnType>;

  abstract updateAlias$(a: IAliasType): Observable<IAliasApiReturnType>;

  abstract deleteAlias$(a: IAliasType): Observable<IAliasApiReturnType>;
}