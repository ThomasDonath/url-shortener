import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/internal/Observable';
import { catchError, map } from "rxjs";

import { IAliasType } from 'src/app/model/AliasesType.interface';
import { IAliasApiReturnType } from '../model/AliasApiReturnType.interface';
import { IAliasesService } from './aliases-service.service.interface';
import { IOAuthService } from './oauth.service.interface';
import { IGetAliasesApiReturnType } from '../model/GetAliasesApiReturnType';

@Injectable({
  providedIn: 'root'
})
export class AliasesService implements IAliasesService {

  private ApiUrl: string = '';

  headers: HttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + this.authService.getAccessToken());

  constructor(private http: HttpClient, private authService: IOAuthService) {
    this.ApiUrl = window.location.origin + '/api/alias/';
  }

  getAll$(
    filterUrl: string,
    filterAlias: string,
    filterOwner: string,
    sortColumn: string = 'full_url',
    sortOrder: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<IGetAliasesApiReturnType[]> {

    let reqOptions = {
      headers: this.headers,
      params: new HttpParams()
        .set('filterUrl', filterUrl)
        .set('filterAlias', filterAlias)
        .set('filterOwner', filterOwner)
        .set('sortColumn', sortColumn)
        .set('sortOrder', sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
    }

    return this.http.get<IGetAliasesApiReturnType[]>(this.ApiUrl, reqOptions);
  }

  createAlias$(a: IAliasType): Observable<IAliasApiReturnType> {
    let reqOptions = {
      headers: this.headers,
      params: {}
    }

    console.debug('service (Insert) called:', a);

    return this.http.post<IAliasApiReturnType>(this.ApiUrl, JSON.stringify(a), reqOptions)
      .pipe(
        catchError(err => {
          console.error(err);
          throw err;
        }));
  }

  updateAlias$(a: IAliasType): Observable<IAliasApiReturnType> {
    let reqOptions = {
      headers: this.headers,
      params: {}
    }

    console.debug('service (Update) called:', a);

    return this.http.put<IAliasApiReturnType>(this.ApiUrl, JSON.stringify(a), reqOptions)
      .pipe(
        catchError(err => {
          console.error(err);
          throw err;
        }));
  }

  deleteAlias$(a: IAliasType): Observable<IAliasApiReturnType> {
    let reqOptions = {
      headers: this.headers,
      params: {}
    }

    console.debug('service Delete called:', a);

    return this.http.delete<IAliasApiReturnType>(this.ApiUrl + a.id, reqOptions)
      .pipe(
        catchError(err => {
          console.error(err);
          throw err;
        }));
  }
}