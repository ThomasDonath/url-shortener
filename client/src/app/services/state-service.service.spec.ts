import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CookieService } from 'ngx-cookie-service';
import { OAuthMockService } from './oauth.service.mock';
import { IOAuthService } from './oauth.service.interface';

import { StateService } from './state-service.service';

describe('StateService', () => {
  let service: StateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      providers:[
        {provide: IOAuthService, useClass: OAuthMockService},
        {provide: CookieService, useClass: CookieService},
      ],
    }).compileComponents();
    service = TestBed.inject(StateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
