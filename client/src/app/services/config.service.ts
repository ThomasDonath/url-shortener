import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Observer, ReplaySubject, tap } from 'rxjs';
import { IConfigService } from './config.service.interface';

@Injectable({
  providedIn: 'root'
})
export class ConfigService implements IConfigService {

  public AadTenantId: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public AadClientId: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public AadScope: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public BrowserTitle: BehaviorSubject<string> = new BehaviorSubject<string>('Shorty');
  public GotConfig: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
    const headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    const base: string = window.location.origin 

    http.get(base + '/clientconfig/', { headers: headers })
      .subscribe(
        (v) => {
          this.AadTenantId.next(v["tenantId"]);
          this.AadClientId.next(v["clientId"]);
          this.AadScope.next(v["scope"]);
          this.BrowserTitle.next(v["browserTitle"]);
          this.GotConfig.next(true);
        }) 
  }
}
