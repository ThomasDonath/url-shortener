import { HttpClient, HttpHandler } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { IOAuthService } from './oauth.service.interface';
import { OAuthMockService } from './oauth.service.mock';

import { StatisticsService } from './statistics.service';

describe('StatisticsService', () => {
  let service: StatisticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: IOAuthService, useClass: OAuthMockService},
        HttpClient, HttpHandler
      ]
    });
    service = TestBed.inject(StatisticsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
