import { BehaviorSubject } from "rxjs";

export abstract class IConfigService {

    public AadTenantId: BehaviorSubject<string>;
    public AadClientId: BehaviorSubject<string>;
    public AadScope: BehaviorSubject<string>;
    public BrowserTitle: BehaviorSubject<string>;
    public GotConfig: BehaviorSubject<boolean>;
}