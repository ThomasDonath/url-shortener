import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/internal/Observable';
import { of } from "rxjs";

import { IAliasType } from 'src/app/model/AliasesType.interface';
import { IAliasApiReturnType } from '../model/AliasApiReturnType.interface';
import { IAliasesService } from './aliases-service.service.interface';
import { IGetAliasesApiReturnType } from '../model/GetAliasesApiReturnType';

@Injectable()
export class AliasesMockService implements IAliasesService {

  headers: HttpHeaders;

  getNumberOfRows$(
    filterUrl: string,
    filterAlias: string,
    filterOwner: string): Observable<number> {
    return of[0];
  }

  getAll$(
    filterUrl: string,
    filterAlias: string,
    filterOwner: string,
    sortColumn: string,
    sortOrder: string,
    pageNumber: number,
    pageSize: number
  ): Observable<IGetAliasesApiReturnType[]> {
    return of()
  }

  createAlias$(a: IAliasType): Observable<IAliasApiReturnType> {
    return null
  }

  updateAlias$(a: IAliasType): Observable<IAliasApiReturnType> {
    return null
  }

  deleteAlias$(a: IAliasType): Observable<IAliasApiReturnType> {
    return null
  }
}