import { Injectable } from "@angular/core";
import { OAuthEvent, OAuthSuccessEvent } from "angular-oauth2-oidc";
import { Observable, of } from "rxjs";
import { IOAuthService } from "./oauth.service.interface";


@Injectable()
export class OAuthMockService implements IOAuthService {
    public events: Observable<OAuthEvent>;

    public getAccessToken(): string {
        return null;
    }
    public getIdToken(): string {
        return null;
    }
    public hasValidAccessToken(): boolean {
        return null;
    }
    public hasValidIdToken(): boolean {
        return false
    }

    public setupAutomaticSilentRefresh(): void { }

    public logOut(): void { }
    public initLoginFlow(): void { }

    public loadUserProfile(): any { }

    public configure(authConfig) { }
    public loadDiscoveryDocumentAndTryLogin() { }

    public getIdentityClaims(): object {
        const claims = {
            prefered_username: 'thomas',
            roles: ['Editor']
        }
        return claims;
    }
}