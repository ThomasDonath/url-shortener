import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IStatisticsApiReturnType } from '../model/StatisticsType.interface';

@Injectable()
export abstract class IStatisticsService {

  abstract getAll$(
    aliasId: string,
    format: string,
    sortBy: string,
    sortOrder: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<IStatisticsApiReturnType>;
}
