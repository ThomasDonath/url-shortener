import { Injectable } from "@angular/core";
import { OAuthEvent } from "angular-oauth2-oidc";
import { Observable } from "rxjs";


@Injectable()
export abstract class IOAuthService {
    public abstract getAccessToken(): string;
    public abstract hasValidAccessToken(): boolean;

    public abstract events: Observable<OAuthEvent>;

    public abstract setupAutomaticSilentRefresh(): void;

    public abstract logOut(): void;

    public abstract initLoginFlow(): void;

    public abstract loadUserProfile(): any; //FIXME signature
    public abstract configure(authConfig);
    public abstract loadDiscoveryDocumentAndTryLogin();

    public abstract getIdentityClaims(); //FIXME signature
    public abstract getIdToken(); //FIXME signature
    public abstract hasValidIdToken(): boolean;

}