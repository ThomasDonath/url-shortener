import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IStatisticsApiReturnType } from '../model/StatisticsType.interface';
import { IOAuthService } from './oauth.service.interface';
import { IStatisticsService } from './statistics.servise.interface';

@Injectable({
  providedIn: 'root'
})
export class StatisticsMockService implements IStatisticsService {
  private ApiUrl: string = '';

  headers: HttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + this.authService.getAccessToken());

  constructor(private http: HttpClient, private authService: IOAuthService) {
    this.ApiUrl = window.location.origin + '/api/alias/';
  }

  getAll$(
    aliasId: string,
    format: string,
    sortBy: string,
    sortOrder: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<IStatisticsApiReturnType> {

    let reqOptions = {
      headers: this.headers,
      params: new HttpParams()
        .set('stats', "json")
        .set('AliasId', aliasId)
        .set('format', format)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
    }

    return of ();
  }
}
