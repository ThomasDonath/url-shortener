import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export abstract class IStateService {

    public abstract isAuthenticated$: Observable<boolean>;
    public abstract isDoneLoading$: Observable<boolean>;

    public abstract UserName: string;
    public abstract GrantedRole: number;

    public abstract isLoggedIn(): boolean;

    abstract logout(): void;

    // unused because of delay in service
    public abstract canActivateProtectedRoutes$: Observable<boolean>;
}
