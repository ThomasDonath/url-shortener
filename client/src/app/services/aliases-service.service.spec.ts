import { TestBed } from '@angular/core/testing';

import { HttpClient, HttpHandler } from '@angular/common/http';

import { IAliasType } from 'src/app/model/AliasesType.interface';
import { AliasesService } from './aliases-service.service';
import { IOAuthService } from './oauth.service.interface';
import { OAuthMockService } from './oauth.service.mock';

describe('AliasesService', () => {
  let service: AliasesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: IOAuthService, useClass: OAuthMockService},
        HttpClient, HttpHandler
      ],
    }).compileComponents();
    service = TestBed.inject(AliasesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
/*
  it('getAll() content of first row should be right', () => {
    let a: IAliasType[]

         service.getAll$()
          .subscribe(rows => {
            a = rows as AliasType[]
          }) 
          a =  lastValueFrom( service.getAll('', 'asc', 0, 20, '').pipe(toArray()))
          expect(a.length).toBe(2) 
    
  });*/
});
