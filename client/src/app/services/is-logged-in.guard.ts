import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { IIsLoggedInGuard } from './is-logged-in.guard.interface';
import { IStateService } from './state-service.service.interface';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedInGuard implements IIsLoggedInGuard {

  constructor(private router: Router, private stateService: IStateService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    console.debug('guard called', this.stateService.isLoggedIn());
    if (!this.stateService.isLoggedIn()) { 
      this.router.navigateByUrl('login'); 
      return false;
    } else {
      return true;
    }
  }
}
