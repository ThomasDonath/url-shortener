import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IStateService } from './state-service.service.interface';

@Injectable()
export class StateMockService implements IStateService {

    public  isAuthenticated$: Observable<boolean>;
    public  isDoneLoading$: Observable<boolean>;

    public  UserName: string;
    public  GrantedRole: number;

    public  isLoggedIn(): boolean{
        return false
    }

    constructor(){};

     logout(): void {}

    // unused because of delay in service
    public  canActivateProtectedRoutes$: Observable<boolean> = null
}
