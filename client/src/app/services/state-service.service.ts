import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { ROLES } from '../model/roles'
import { IOAuthService } from './oauth.service.interface';
import { IStateService } from './state-service.service.interface';

@Injectable({
  providedIn: 'root'
})
export class StateService implements IStateService {

  private isAuthenticatedSubject$ = new BehaviorSubject<boolean>(false);
  public isAuthenticated$ = this.isAuthenticatedSubject$.asObservable();

  private isDoneLoadingSubject$ = new BehaviorSubject<boolean>(false);
  public isDoneLoading$ = this.isDoneLoadingSubject$.asObservable();

  public UserName: string = '';
  public GrantedRole: number = 0;

  public isLoggedIn(): boolean {
    return this.authService.hasValidAccessToken()
  }

  constructor(private authService: IOAuthService, private router: Router, private cookieService: CookieService) {
    if (this.authService.events === undefined) {
      console.error("StateService.authService is undefined")
    } else {
      this.authService.events
        .subscribe(event => {
          console.debug('OAuth service event', event)
        });

      this.authService.events
        .subscribe(event => {
          let isAuthenticated = this.authService.hasValidAccessToken();
          console.debug(`OAuth service Event ${event.type}; isAuthenticated=${isAuthenticated}`)
          this.isAuthenticatedSubject$.next(isAuthenticated);
        });

      this.authService.events
        .pipe(filter(e => ['token_received'].includes(e.type)))
        .pipe(tap(_ => console.debug('token received')))
        .subscribe(e => this.authService.loadUserProfile());

      this.authService.events
        .pipe(filter(e => ['session_terminated', 'session_error'].includes(e.type)))
        .subscribe(e => this.router.navigateByUrl('login'));

      this.authService.setupAutomaticSilentRefresh();
    }
  }

  logout() {
    this.cookieService.delete('auth', '/')
    this.cookieService.deleteAll('/'); 
    this.authService.logOut();
  }

  // unused because of delay in service
  public canActivateProtectedRoutes$: Observable<boolean> = combineLatest([
    this.isAuthenticated$,
    this.isDoneLoading$
  ])
    .pipe(tap(v => { console.debug('subs1', v) })).pipe(map(values => values.every(b => b)))
    .pipe(map(v => v));
}
