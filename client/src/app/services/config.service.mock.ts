import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { IConfigService } from "./config.service.interface";

@Injectable()
export class ConfigMockService implements IConfigService{

    public AadTenantId: BehaviorSubject<string> = new BehaviorSubject<string>('');
    public AadClientId: BehaviorSubject<string> = new BehaviorSubject<string>('');
    public AadScope: BehaviorSubject<string> = new BehaviorSubject<string>('');
    public BrowserTitle: BehaviorSubject<string> = new BehaviorSubject<string>('Shorty');
    public GotConfig: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
}