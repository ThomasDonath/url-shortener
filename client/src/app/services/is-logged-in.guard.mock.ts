import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { IIsLoggedInGuard } from './is-logged-in.guard.interface';

@Injectable()
export class IsLoggedInMockGuard implements IIsLoggedInGuard {

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
            return false
    }
}
