import { APP_BASE_HREF } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { provideHttpClient, withInterceptorsFromDi } from "@angular/common/http";
import { LoggedInComponent } from './components/logged-in/logged-in.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TitleStrategy } from '@angular/router';

import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from "@angular/material/input";

import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSelectModule } from "@angular/material/select";
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';

import { AliasesService } from './services/aliases-service.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ConfigService } from './services/config.service';
import { CookieService } from 'ngx-cookie-service';
import { EditorComponent } from './components/editor/editor.component';
import { IAliasesService } from './services/aliases-service.service.interface';
import { IConfigService } from './services/config.service.interface';
import { IOAuthService } from './services/oauth.service.interface';
import { IStateService } from './services/state-service.service.interface';
import { IStatisticsService } from './services/statistics.servise.interface';
import { ListComponent } from './components/list/list.component';
import { LoginComponent } from './components/login/login.component'
import { OAuthModule, OAuthService } from 'angular-oauth2-oidc';
import { StateService } from './services/state-service.service'
import { StatisticsComponent } from './components/statistics/statistics.component';
import { StatisticsService } from './services/statistics.service';
import { TemplatePageTitleStrategy } from './app-title-strategie';

@NgModule({
    declarations: [
        AppComponent,
        EditorComponent,
        ListComponent,
        LoggedInComponent,
        LoginComponent,
        StatisticsComponent,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent],
    imports: [AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        MatDatepickerModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatIconModule,
        MatInputModule,
        MatNativeDateModule,
        MatMomentDateModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatSortModule,
        MatTableModule,
        MatTooltipModule,
        ReactiveFormsModule,
        OAuthModule.forRoot({
            resourceServer: {
                sendAccessToken: true,
                allowedUrls: ['http://localhost:4200/api/']
            }
        })], providers: [
            { provide: APP_BASE_HREF, useValue: '/client/' },
            { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
            { provide: MAT_DATE_LOCALE, useValue: 'de-DE' },
            { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }, // same as server
            { provide: TitleStrategy, useClass: TemplatePageTitleStrategy },
            CookieService,
            { provide: IOAuthService, useClass: OAuthService },
            { provide: IAliasesService, useClass: AliasesService },
            { provide: IStatisticsService, useClass: StatisticsService },
            { provide: IStateService, useClass: StateService },
            { provide: IConfigService, useClass: ConfigService },
            provideHttpClient(withInterceptorsFromDi())
        ]
})

export class AppModule { }