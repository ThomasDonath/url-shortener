import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { OAuthMockService } from './services/oauth.service.mock';
import { IOAuthService } from './services/oauth.service.interface';
import { IConfigService } from './services/config.service.interface';
import { ConfigMockService } from './services/config.service.mock';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        { provide: IOAuthService, useClass: OAuthMockService },
        { provide: IConfigService, useClass: ConfigMockService }
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Shorty'`, () => {
    expect(app.title).toEqual('Shorty');
  });
});
