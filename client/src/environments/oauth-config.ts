import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  redirectUri: `${window.location.origin}/client/logged-in/`,

  postLogoutRedirectUri: `${window.location.origin}/client/login/`,

  responseType: 'code',

  preserveRequestedRoute: true,

  // required for AzureAD
  strictDiscoveryDocumentValidation: false,

  showDebugInformation: false,
}